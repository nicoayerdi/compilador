package assembler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import analizadorSintactico.Parser;
import compilador.Mensajes;

public class GeneradorAssembler {

	private String saltofinal = "";
	
	private Parser parser;
	private TablaSimbolos tabla;
	private List<String> polaca;
	private Mensajes mensajes;
	private AnalizadorLexico al;
	
	public GeneradorAssembler (Parser p, Mensajes m, AnalizadorLexico al){
		this.parser = p;
		tabla = parser.getTablaSimbolos();
		polaca = p.getPolaca();
		this.mensajes = m;
		this.al = al;
	}
	
	public String generarCodigoAssembler(){
		String codigo = "";
		codigo += ".386\n.model flat, stdcall\noption casemap :none\ninclude \\masm32\\include\\windows.inc\ninclude \\masm32\\include\\kernel32.inc\ninclude \\masm32\\include\\user32.inc\nincludelib <c:\\masm32\\lib\\kernel32.lib>\nincludelib \\masm32\\lib\\user32.lib\n.data\n";
		codigo += generarDeclaraciones();
		codigo += ".code\n";
		codigo += generarEjecuciones();
		if(!saltofinal.equals(""))
			codigo += saltofinal+ ": \n";
		codigo += "invoke ExitProcess, 0\nend start";
		return codigo;
	}

	private String generarDeclaraciones() {
		String codigo = "";
		int constantes = 1;
		int cadenas = 1;
		for (String lexema: tabla.getKeySet()) {
			
			if (tabla.getEntradaTS(lexema).getTipo() != null && tabla.getEntradaTS(lexema).getUso() != null) 
				if (tabla.getEntradaTS(lexema).getUso().equals("Nombre de Variable") || tabla.getEntradaTS(lexema).getUso().equals("Nombre de Atributo")) {
					
					if (tabla.getEntradaTS(lexema).getTipo().equals("int")) {
						tabla.getEntradaTS(lexema).setNombreAssembler("_" + lexema);
						codigo += "_" + lexema + " DW ? \n";						// Entero definido de 2 bytes
					} else if (tabla.getEntradaTS(lexema).getTipo().equals("double")) {
						tabla.getEntradaTS(lexema).setNombreAssembler("_" + lexema);
						codigo += "_" + lexema + " DQ ? \n";						// Double definido de 8 bytes
					}
					
				} else if (tabla.getEntradaTS(lexema).getUso().equals("Constante Entera")) {
					tabla.getEntradaTS(lexema).setNombreAssembler(lexema);
				} else if (tabla.getEntradaTS(lexema).getUso().equals("Constante Double")) {
					tabla.getEntradaTS(lexema).setNombreAssembler("__cte" + constantes);
					codigo += "__cte" + constantes + " DQ " + lexema + "\n";		// Double definido de 8 bytes
					constantes++;
				} else if (tabla.getEntradaTS(lexema).getUso().equals("Variable Auxiliar")) {
					
					if (tabla.getEntradaTS(lexema).getTipo().equals("int")) {
						tabla.getEntradaTS(lexema).setNombreAssembler(lexema);
						codigo += lexema + " DW ? \n";								// Entero definido de 2 bytes
					} else if (tabla.getEntradaTS(lexema).getTipo().equals("double")) {
						tabla.getEntradaTS(lexema).setNombreAssembler(lexema);
						codigo += lexema + " DQ ? \n";								// Double definido de 8 bytes
					}
					
				} else if (tabla.getEntradaTS(lexema).getUso().equals("Cadena de caracteres")) {
					tabla.getEntradaTS(lexema).setNombreAssembler("cad" + cadenas);
					codigo += "cad" + cadenas + " DB " + "\"" + tabla.getEntradaTS(lexema).getLexema() + "\" , 0\n"; 		// Se una \" para imprimir comillas dobles
					cadenas++;
				}
					
		}
	
		codigo += "error_overflow DB \"Se produjo un overflow en una suma\" , 0\n";
		codigo += "error_overflow_mul DB \"Se produjo un overflow en la multiplicacion\" , 0\n";
		codigo += "error_division DB \"Se produjo una division por 0\" , 0\n";
		codigo += "error_titulo DB \"ERROR\" , 0\n";
		codigo += "imprimir_titulo db \"Mensaje\" , 0\n";
		codigo += "__cteceroDW DW 0 \n";
		codigo += "__cteceroDQ DQ 0.0 \n";
		codigo += "__ctemaxdoublepos DQ 1.7976931348623157E308 \n";
		
		
		return codigo;
	}
	
	private String generarEjecuciones() {	
		String codigo = "";
		Stack<String> consumidos = new Stack<String>();
		
		int cantidadMetodos = parser.getcantMetodos();
		String operador = "";
		boolean start = false;
		
		int cursor = 0;
		int nroSalto = 1;
		
		String nombreMetodo;
		
		int dirSalto = 0;
		int dirSaltoAtras = 0;
		
		HashMap<Integer,String> saltosAtras = new HashMap<Integer,String>();
		
		// Primera pasada: marcar datos en las variables a las que hay que volver
		for (int i = 0; i < polaca.size(); i++) {
			if (polaca.get(i).equals("BI")) {
				Integer pos = Integer.valueOf(polaca.get(i-1));
				if (pos < i) {
					String valorPrevio = polaca.get(pos);
					saltosAtras.put(pos, "Label" + nroSalto);
					nroSalto++;
					polaca.add(pos, (valorPrevio + "$"));
					polaca.remove(pos + 1);
				}
			}
		}
		
		ArrayList<Par> saltosAdelante = new ArrayList<Par>();
		
		while (cursor < polaca.size()){
			
			consumidos.push(polaca.get(cursor));
			cursor++;
			
			if (!start && cantidadMetodos == 0) {
				codigo += "start: \n";
				start = true;
			}
			
			// Saltos incondicionales
			if (consumidos.peek().contains("$")) {
				codigo += saltosAtras.get(cursor - 1) + ": \n";
				String aux = consumidos.pop().replace("$", "");
				consumidos.push(aux);
				polaca.add(cursor - 1, aux);
				polaca.remove(cursor);
			}
			
			// Saltos BF y BI para adelante
			// Me fijo si la posicion que estoy viendo es una direccion a la que se salta en la lista de saltos
			for (int j=0; j < saltosAdelante.size(); j++) 
				if (saltosAdelante.get(j).getPos() == cursor - 1)
					codigo += saltosAdelante.get(j).getLabel() + ": \n";
			
			
			if (consumidos.peek().equals(":=")){
				consumidos.pop();
				String izq = consumidos.pop();
				String der = consumidos.pop();

				tabla.getEntradaTS(izq).setLexema(tabla.getEntradaTS(der).getLexema()); 
				if (tabla.getEntradaTS(izq).getTipo().equals("int") && tabla.getEntradaTS(der).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
					codigo += "MOV " + tabla.getEntradaTS(izq).getNombreAssembler() + ", BX \n";
				}
				if (tabla.getEntradaTS(izq).getTipo().equals("double") && tabla.getEntradaTS(der).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
					codigo += "FSTP " + tabla.getEntradaTS(izq).getNombreAssembler() + "\n";				
				}
				if (!tabla.getEntradaTS(izq).getTipo().equals(tabla.getEntradaTS(der).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), izq, der);
				}
			}
			else if (consumidos.peek().equals("+")){
				consumidos.pop();
				String op1 = consumidos.pop();
				String op2 = consumidos.pop();
				String dir = polaca.get(cursor);
				cursor++;
				if (tabla.getEntradaTS(op1).getTipo().equals("int") && tabla.getEntradaTS(op2).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "ADD BX, " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "JNO label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_overflow, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";
					codigo += "MOV " + dir + ", BX \n";
					nroSalto++;
				}
				if (tabla.getEntradaTS(op1).getTipo().equals("double") && tabla.getEntradaTS(op2).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "FADD " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "FCOM " + "__ctemaxdoublepos \n";
					codigo += "FSTSW AX \n";
					codigo += "SAHF \n";
					codigo += "JB label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_overflow, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";
					codigo += "FSTP " + dir + "\n";
					nroSalto++;
				}
				if (!tabla.getEntradaTS(op1).getTipo().equals(tabla.getEntradaTS(op2).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), op1, op2);
				}
				consumidos.push(dir);	
			}	
			else if (consumidos.peek().equals("-")){
				consumidos.pop();
				String op2 = consumidos.pop();
				String op1 = consumidos.pop();
				String dir = polaca.get(cursor);
				cursor++;
				if (tabla.getEntradaTS(op1).getTipo().equals("int") && tabla.getEntradaTS(op2).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "SUB BX, " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "MOV " + dir + ", BX \n";
				}
				if (tabla.getEntradaTS(op1).getTipo().equals("double") && tabla.getEntradaTS(op2).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "FSUB " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";	
					codigo += "FSTP " + dir + "\n";
				}
				if (!tabla.getEntradaTS(op1).getTipo().equals(tabla.getEntradaTS(op2).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), op1, op2);
				}
				consumidos.push(dir);	
			}	
			else if (consumidos.peek().equals("/")){
				consumidos.pop();
				String op2 = consumidos.pop();
				String op1 = consumidos.pop();			
				String dir = polaca.get(cursor);
				cursor++;
				if (tabla.getEntradaTS(op1).getTipo().equals("int") && tabla.getEntradaTS(op2).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(op2).getNombreAssembler() +"\n"; 
					codigo += "CMP BX, __cteceroDW \n";
					codigo += "JNE label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_division, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";
					codigo += "MOV AX, " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "CWD \n";
					codigo += "MOV CX, " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "IDIV CX \n";
					codigo += "MOV " + dir + ", AX \n";
					nroSalto++;
				}
				if (tabla.getEntradaTS(op1).getTipo().equals("double") && tabla.getEntradaTS(op2).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(op2).getNombreAssembler() +"\n"; 
					codigo += "FTST \n";
					codigo += "FSTSW AX \n";
					codigo += "SAHF \n";
					codigo += "JNZ label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_division, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";	
					codigo += "FDIVR " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "FSTP " + dir + "\n";
					nroSalto++;
				}
				if (!tabla.getEntradaTS(op1).getTipo().equals(tabla.getEntradaTS(op2).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), op1, op2);
				}
				consumidos.push(dir);
			
			}	
			else if (consumidos.peek().equals("*")){
				consumidos.pop();
				String op2 = consumidos.pop();
				String op1 = consumidos.pop();
				String dir = polaca.get(cursor);
				cursor++;
				if (tabla.getEntradaTS(op1).getTipo().equals("int") && tabla.getEntradaTS(op2).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "IMUL BX, " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "JNO label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_overflow_mul, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";
					codigo += "MOV " + dir + ", BX \n";
					nroSalto++;
				}
				if (tabla.getEntradaTS(op1).getTipo().equals("double") && tabla.getEntradaTS(op2).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "FMUL " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "FCOM " + "__ctemaxdoublepos \n";
					codigo += "FSTSW AX \n";
					codigo += "SAHF \n";
					codigo += "JB label" + nroSalto + "\n";
					codigo += "invoke MessageBox, NULL, addr error_overflow_mul, addr error_titulo, MB_OK\n";
					codigo += "invoke ExitProcess, 0 \n";
					codigo += "label" + nroSalto + ": \n";
					codigo += "FSTP " + dir + "\n";		
					nroSalto++;
				}
				if (!tabla.getEntradaTS(op1).getTipo().equals(tabla.getEntradaTS(op2).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), op1, op2);
				}
				consumidos.push(dir);	
			}	
			else if (consumidos.peek().equals("print")){
				consumidos.pop();
				String cadena = consumidos.pop();
				codigo += "invoke MessageBox, NULL, addr " + tabla.getEntradaTS("#" + cadena).getNombreAssembler() + ", addr imprimir_titulo, MB_OK\n";					
			}
			else if (consumidos.peek().equals("BI")){
				if (Integer.parseInt(polaca.get(cursor-2)) > cursor-1){
					consumidos.pop();
					dirSalto = Integer.parseInt(consumidos.pop());
					Par par = new Par(dirSalto,"label" + nroSalto);
					codigo += "JMP label" + nroSalto + "\n";
					saltosAdelante.add(par);
					
					// necesario para tener el ultimo label en caso de finalizar con un IF o FOR
					if (dirSalto >= polaca.size()){
						saltofinal = "label" + nroSalto;							
					}
					
					nroSalto++;
				}
				else{
					consumidos.pop();
					dirSaltoAtras = Integer.parseInt(consumidos.pop());
					codigo += "JMP " + saltosAtras.get(dirSaltoAtras) + "\n";
				}
			}
			else if (consumidos.peek().equals("BF")){
				consumidos.pop();
				dirSalto = Integer.parseInt(consumidos.pop());
				Par par = new Par(dirSalto,"label" + nroSalto);
				saltosAdelante.add(par);
				
				// necesario para tener el ultimo label en caso de finalizar con un IF o FOR
				if (dirSalto >= polaca.size()){
					saltofinal = "label" + nroSalto;							
				}
				
				if (operador.equals("<")){
					codigo += "JGE label" + nroSalto + "\n";
				}
				else if (operador.equals("<=")){
					codigo += "JG label" + nroSalto + "\n";
				}
				else if (operador.equals(">")){
					codigo += "JLE label" + nroSalto + "\n";
				}
				else if (operador.equals(">=")){
					codigo += "JL label" + nroSalto + "\n";
				}
				else if (operador.equals("==")){
					codigo += "JNE label" + nroSalto + "\n";
				}
				else if (operador.equals("<>")){
					codigo += "JE label" + nroSalto + "\n";
				}
				nroSalto++;
			}	
			else if (consumidos.peek().equals("<") || consumidos.peek().equals("<=") || consumidos.peek().equals(">") || consumidos.peek().equals(">=") || consumidos.peek().equals("==") || consumidos.peek().equals("<>")){
				operador = consumidos.pop();
				String op2 = consumidos.pop();
				String op1 = consumidos.pop();
				
				if (tabla.getEntradaTS(op1).getTipo().equals("int") && tabla.getEntradaTS(op2).getTipo().equals("int")){
					codigo += "MOV BX, " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "MOV CX, " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "CMP BX, CX \n";
				}
				if (tabla.getEntradaTS(op1).getTipo().equals("double") && tabla.getEntradaTS(op2).getTipo().equals("double")){
					codigo += "FLD " + tabla.getEntradaTS(op1).getNombreAssembler() + "\n";
					codigo += "FCOM " + tabla.getEntradaTS(op2).getNombreAssembler() + "\n";
					codigo += "FSTSW AX \n";
					codigo += "SAHF \n";
				}
				if (!tabla.getEntradaTS(op1).getTipo().equals(tabla.getEntradaTS(op2).getTipo())){
					mensajes.errorTipos(al.getMensaje(66), op1, op2);
				}
			}
			else if (consumidos.peek().equals("IM")){
				consumidos.pop();
				nombreMetodo = consumidos.pop();
				codigo += "label" + nombreMetodo + ": \n";
			}
			else if (consumidos.peek().equals("FM")){
				consumidos.pop();
				codigo += "RET \n";
				cantidadMetodos--;
			}
			else if (consumidos.peek().equals("LM")){
				consumidos.pop();
				nombreMetodo = consumidos.pop();
				if (nombreMetodo.contains("@")) {
					String objeto = nombreMetodo.substring(0, nombreMetodo.indexOf("@"));
					for (String a: tabla.getKeySet()) {
						if (a.contains(objeto + "@") && tabla.getEntradaTS(a).getUso().equals("Nombre de Atributo")) {
							String der = a;
							String izq = a.substring(a.indexOf("@") + 1);
							if (tabla.getEntradaTS(a).getTipo().equals("int")) {
								codigo += "MOV BX, " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
								codigo += "MOV " + tabla.getEntradaTS(izq).getNombreAssembler() + ", BX \n";
							} else if (tabla.getEntradaTS(a).getTipo().equals("double")) {
								codigo += "FLD " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
								codigo += "FSTP " + tabla.getEntradaTS(izq).getNombreAssembler() + "\n";	
							}
						}
					}
					
					codigo += "CALL label"+ nombreMetodo.substring(nombreMetodo.indexOf("@") + 1) + "\n";
					
					for (String a: tabla.getKeySet()) {
						if (a.contains(objeto + "@") && tabla.getEntradaTS(a).getUso().equals("Nombre de Atributo")) {
							String izq = a;
							String der = a.substring(a.indexOf("@") + 1);
							if (tabla.getEntradaTS(a).getTipo().equals("int")) {
								codigo += "MOV BX, " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
								codigo += "MOV " + tabla.getEntradaTS(izq).getNombreAssembler()  + ", BX \n";
							} else if (tabla.getEntradaTS(a).getTipo().equals("double")) {
								codigo += "FLD " + tabla.getEntradaTS(der).getNombreAssembler() + "\n";
								codigo += "FSTP " + tabla.getEntradaTS(izq).getNombreAssembler()  + "\n";	
							}
						}
					}
				}
				else
					codigo += "CALL label"+ nombreMetodo + "\n";

			}
			
			
			
		}
		
		return codigo;
	}

}
