package assembler;

public class Par {
	
	int pos;
	String label;
	
	public Par(int pos, String label){
		this.pos = pos;
		this.label = label;
	}
	
	public int getPos(){
		return this.pos;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public boolean equals(Object o){
		Par p = (Par) o;
		return (p.getPos() == this.pos && p.getLabel().equals(this.label));
	}
}
