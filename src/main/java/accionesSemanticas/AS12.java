package accionesSemanticas;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS12 extends AccSemantica{

	// Vacia el Token e informa error.
	
	private Mensajes ms;
    private AnalizadorLexico al;
    public static final Short COMODIN = 274;
    
    public AS12 (Mensajes m, AnalizadorLexico a) {
        ms = m;
        al = a;
    }
	
    private int entrada (int caracter) {
    	return al.getColumna(caracter);
    }
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token = new Token();
        int aux = entrada(caracter); // Me dice el tipo de mensaje
        token.seAgregoCaracterLeido();
        if (aux == 22){				 // Cualquier caracter no definido
            token.seAgregoCaracterLeido();
            ms.error(al.getNroLinea(), al.getMensaje(2));
        }
        else
             ms.error(al.getNroLinea(), al.getMensaje(3));
        token.setId(COMODIN);
        return token;
	}

}
