package accionesSemanticas;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS10 extends AccSemantica{

	// Empaqueta el token controlando el rango de un Entero y sin consumir el ultimo caracter
	
	public final static short CTEENTERO = 272;
	private TablaSimbolos ts;
	private AnalizadorLexico al;
	private Mensajes ms;
	
	public AS10(TablaSimbolos ts, AnalizadorLexico al, Mensajes ms) {
		this.ts = ts;
		this.al = al;
		this.ms = ms;
	}
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.setId(CTEENTERO);
		token.setTipo("Constante Entera");
		Long e = Long.valueOf(token.getLexema());
		
		token.setLexema(String.valueOf(e));
		if(e >= 0 && e <= Short.MAX_VALUE + 1) { 	// Reconoce enteros del 0 al 32768
			if(ts.contieneLexema(token.getLexema())) {
				ts.getEntradaTS(token.getLexema()).incrementarCont();
				token.setEntradaTS(ts.getEntradaTS(token.getLexema()));
			}
			else {
				ts.addETS(token.getLexema(), token.getETS());
				ts.getEntradaTS(token.getLexema()).setUso("Constante Entera");
				ts.getEntradaTS(token.getLexema()).setTipo("int");
			}
			ms.token(al.getNroLinea(),token.getTipo(),token.getLexema());
		}
		else 
			ms.error(al.getNroLinea(), al.getMensaje(4)); 
		
		token.noSeAgregoCaracterLeido();
		return token;
	}

}
