package accionesSemanticas;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS11 extends AccSemantica{

	// Empaqueta el token, controlando el rango de un dobule, no adiciona el ultimo simbolo 
	
    public static final Short CTEDOUBLE = 273;
    private Mensajes ms;
    private AnalizadorLexico al;
    private TablaSimbolos tabla;
	public static final double MIN_DOUBLE= 2.2250738585072014E-308;
	public static final double MAX_DOUBLE= 1.7976931348623157E308;
    
    public AS11(Mensajes m, AnalizadorLexico a, TablaSimbolos t){
        ms = m;
        al = a;
        tabla = t;
    }
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.setId(CTEDOUBLE);
		token.setTipo("Constante Double");
		double d = Double.valueOf(token.getLexema().replace('d','e').replace('D','E'));

		token.setLexema(String.valueOf(d)); 
		if ((Math.abs(d) > MIN_DOUBLE && Math.abs(d) < MAX_DOUBLE) || (d == 0)){
			if (tabla.contieneLexema(token.getLexema())){
				tabla.getEntradaTS(token.getLexema()).incrementarCont();
				token.setEntradaTS(tabla.getEntradaTS(token.getLexema()));
			}
			else {
				tabla.addETS(token.getLexema(), token.getETS());
				tabla.getEntradaTS(token.getLexema()).setUso("Constante Double"); 
				tabla.getEntradaTS(token.getLexema()).setTipo("double"); 
			}
			ms.token(al.getNroLinea(),token.getTipo() ,token.getLexema());
		}
		else 
			ms.error(al.getNroLinea(), al.getMensaje(5));

		token.noSeAgregoCaracterLeido();
		return token;
	}

	
	
}
