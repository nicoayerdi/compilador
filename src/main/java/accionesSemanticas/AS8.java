package accionesSemanticas;

import java.util.Hashtable;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS8 extends AccSemantica{

	// Empaqueta el token controlando la longitud sin consumir el simbolo leido
	
	public static final Short IF = 257; 
	public static final Short ELSE = 258;
	public static final Short END_IF = 259;
	public static final Short PRINT = 260; 
	public static final Short INT = 261;
	public static final Short BEGIN = 262;
	public static final Short END = 263;
	public static final Short DOUBLE = 264; 
    public static final Short WHILE = 266; 
    public static final Short DO = 267;
    public static final Short CLASS = 268;
    public static final Short PUBLIC = 269;
    public static final Short PRIVATE = 270;
    
    public static final Short VOID = 278;
    public static final Short EXTENDS = 279;
	
    public static final Short ID = 271;
    
    private TablaSimbolos ts;
    private Mensajes ms;
    private AnalizadorLexico al;
    private Hashtable<String, Short> palabrasReservadas;
    
    private void verificarLongitudString(Token token) { // Si pasa el limite lo trunca y warning 
    	if (token.getLongitud() > 25) {
    		ms.warning("Linea " + al.getNroLinea() + ": Warning : El identificador '" + token.getLexema() + "' ha sido truncado"); 
    		token.truncarId();
    	}
    }
    
    public AS8(TablaSimbolos t, Mensajes m, AnalizadorLexico a) {
    	ts = t;
    	ms = m;
    	al = a;
    	
    	palabrasReservadas = new Hashtable<String, Short>();
        palabrasReservadas.put("if", IF); 
        palabrasReservadas.put("else", ELSE);
        palabrasReservadas.put("end_if", END_IF);
        palabrasReservadas.put("print", PRINT);
        palabrasReservadas.put("int", INT);
        palabrasReservadas.put("begin", BEGIN);
        palabrasReservadas.put("end", END);
        palabrasReservadas.put("double", DOUBLE);
        palabrasReservadas.put("while", WHILE);
        palabrasReservadas.put("do", DO);
        palabrasReservadas.put("class", CLASS);
        palabrasReservadas.put("public", PUBLIC);
        palabrasReservadas.put("private", PRIVATE);
        palabrasReservadas.put("void", VOID);
        palabrasReservadas.put("extends", EXTENDS);
        

	}
    
    @Override
    public Token ejecutar(Token token, char caracter) {

    	if (ts.contieneLexema(token.getLexema())){                       // Si el token es un lexema que ya esta agregado a la tabla de simbolos
    		
    		ts.getEntradaTS(token.getLexema()).incrementarCont();
    		token.setEntradaTS(ts.getEntradaTS(token.getLexema()));		 // Le seteo al token la entrada de la tabla de simbolos
    		token.noSeAgregoCaracterLeido();                             // Seteo que el ultimo caracter leido no fue agregado al token

    		ms.token(al.getNroLinea(),ts.getEntradaTS(token.getLexema()).getTipo(),token.getLexema());
    		return token;
    	}
    	else {
    		if (!ts.esPalabraReservada(token.getLexema())){              // Si el token no es una palabra reservada
    			verificarLongitudString(token);                          // Verifico la longitud del token
    			token.setId(ID);                                         // Seteo el tipo como identificador
    			token.setTipo("Identificador");
    			token.getETS().setTipo("Identificador");
                ts.addETS(token.getLexema(), token.getETS()); 
    		}
    		else {
    			token.setId((Short) palabrasReservadas.get(token.getLexema())); // Devuelvo el token con la palabra reservada, sin guardarla en la tabla de simbolos
    			token.setTipo("Palabra Reservada");
    			
    		}

    		ms.token(al.getNroLinea(),token.getTipo(),token.getLexema());
    		token.noSeAgregoCaracterLeido();                              //Seteo que el ultimo caracter leido no fue agregado al token
    		return token;
    	}

    }
    
	
}
