package accionesSemanticas;

import java.util.Hashtable;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS4 extends AccSemantica{

	// Adiciona el simbolo leido y empaqueta el token.
	
	public static final Short COMPARADOR = 275;
    public static final Short ASIGNACION = 276;

    private Mensajes ms;
    private AnalizadorLexico al;
    private Hashtable <String, Short> simbolos;
    
    public AS4(Mensajes m, AnalizadorLexico a) {
    	 ms = m;
         al = a;
         simbolos = new Hashtable<String, Short>(); 
         simbolos.put(">=", COMPARADOR);
         simbolos.put("<=", COMPARADOR);
         simbolos.put("<>", COMPARADOR);
         simbolos.put("==", COMPARADOR);
         simbolos.put(":=", ASIGNACION);
    }
    
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.agregarCaracter(caracter);
		ms.token(al.getNroLinea(),null,token.getLexema());
		token.setId((Short)simbolos.get(token.getLexema()));
        return token;
	}

	
	
}
