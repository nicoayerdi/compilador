package accionesSemanticas;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS9 extends AccSemantica{

	// Empaqueta el token consumiendo el simbolo, pero sin adicionarlo.
	// Este es el caso del }
	
	public static final Short CADENACARACTERES = 277;
    private Mensajes ms;
    private AnalizadorLexico al;
    private TablaSimbolos ts;
	
    public AS9 (Mensajes m, AnalizadorLexico a, TablaSimbolos t){
        ms = m;
        al = a;
        ts = t;
    }
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.setId(CADENACARACTERES);
		token.setTipo("Cadena de Caracteres");
        ms.token(al.getNroLinea(),token.getTipo(),token.getLexema());
        if (!ts.contieneLexema("#" + token.getLexema())){
            ts.addETS("#" + token.getLexema(), token.getETS());
            token.getETS().setTipo("Cadena de caracteres");
            token.getETS().setUso("Cadena de caracteres");
        }
        else {
        	ts.getEntradaTS("#" + token.getLexema()).incrementarCont();
        	token.setEntradaTS(ts.getEntradaTS("#" + token.getLexema()));
        }
        token.seAgregoCaracterLeido();
        return token;
	}

	
	
}
