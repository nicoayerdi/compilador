package accionesSemanticas;

import java.util.Hashtable;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS5 extends AccSemantica{

	//  Empaqueta el token sin adicionar el simbolo y sin consumirlo
	
	public static final Short COMPARADOR = 275;
	
	private Mensajes ms;
    private AnalizadorLexico al;
    private Hashtable <String, Short> simbolos;
	
    public AS5(Mensajes m, AnalizadorLexico a) {
    	ms = m;
    	al = a;
    	simbolos = new Hashtable<String, Short>(); 
    	simbolos.put("<", COMPARADOR);
    	simbolos.put(">", COMPARADOR);
    	simbolos.put(".", new Short((short)'.'));
    }
    
	@Override
	public Token ejecutar(Token token, char caracter) {
		ms.token(al.getNroLinea(),null,token.getLexema());
        token.noSeAgregoCaracterLeido();
        token.setId((Short)simbolos.get(token.getLexema()));
        return token;
	}

	
	
}
