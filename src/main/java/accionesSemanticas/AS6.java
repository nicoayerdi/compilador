package accionesSemanticas;

import analizadorLexico.Token;

public class AS6 extends AccSemantica{

	// Lee un simbolo sin adicionarlo a ningun token.
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.seAgregoCaracterLeido();
		return token;
	}

	
}
