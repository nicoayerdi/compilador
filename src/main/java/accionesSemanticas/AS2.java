package accionesSemanticas;

import analizadorLexico.Token;

public class AS2 extends AccSemantica{

	// Agrega al token recibido un caracter
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token.agregarCaracter(caracter);
		return token;
	}
	
}
