package accionesSemanticas;

import java.util.Hashtable;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.Token;
import compilador.Mensajes;

public class AS3 extends AccSemantica{

	// Inicializa un token, adiciona el simbolo leido y empaqueta el token
	
	private Mensajes ms;
	private AnalizadorLexico al;
	private Hashtable <String, Short> simbolos;
	
	public AS3(Mensajes m, AnalizadorLexico a){
        ms = m;
        al = a;
        simbolos = new Hashtable<String, Short>(); 
        simbolos.put("+", new Short((short)'+'));
        simbolos.put("-", new Short((short)'-'));
        simbolos.put("*", new Short((short)'*'));
        simbolos.put("/", new Short((short)'/'));
        simbolos.put(",", new Short((short)','));
        simbolos.put(";", new Short((short)';'));
        simbolos.put(")", new Short((short)')'));
        simbolos.put("(", new Short((short)'('));
    }
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token = new Token();
        token.agregarCaracter(caracter);
        ms.token(al.getNroLinea(),null,token.getLexema());
        token.setId((Short)simbolos.get(token.getLexema())); // seteo la id, osea el numero ascii del operador 
        return token;
	}

}
