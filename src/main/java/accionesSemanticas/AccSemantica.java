package accionesSemanticas;

import analizadorLexico.Token;

public abstract class AccSemantica {

	public abstract Token ejecutar (Token token, char caracter);
	
}
