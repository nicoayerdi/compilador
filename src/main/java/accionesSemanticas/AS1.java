package accionesSemanticas;

import analizadorLexico.Token;

public class AS1 extends AccSemantica{

	// Inicializa un Token y agrega un caracter leido
	
	@Override
	public Token ejecutar(Token token, char caracter) {
		token = new Token();
		token.agregarCaracter(caracter);
		return token;
	}

}
