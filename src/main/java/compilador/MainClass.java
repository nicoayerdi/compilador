package compilador;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import analizadorLexico.AnalizadorLexico;
import analizadorSintactico.Parser;
import assembler.GeneradorAssembler;

public class MainClass implements Mensajes{

	private AnalizadorLexico analizadorLexico;
	private StringBuilder textoToken = new StringBuilder();
	private StringBuilder textoWarning = new StringBuilder();
	private StringBuilder textoError = new StringBuilder();
	private StringBuilder textoSintactica = new StringBuilder();
	private String tablaSimbolos;
	private String codigo = null;
	
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	@SuppressWarnings("resource")
	protected void analizarCodigo(String parametro) throws IOException {
		
		String codigoFuente = readFile(parametro, Charset.defaultCharset());
		
		analizadorLexico = new AnalizadorLexico(codigoFuente.replace("\r", "") + " " +(char)255, this);
		
		Parser parser = new Parser();
		parser.setLexico(analizadorLexico);
		parser.setMensajes(this);
		parser.run();
		
		List<String> pila = parser.getPolaca();
				
		try {
			String outPut;
			outPut = new File (MainClass.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
			outPut = outPut.replace("EjecutableCompilador.jar", "SalidaCompilador.txt");
			
//			File nameFileOutput = new File ("src/main/java/compilador/Salida.txt"); // cambiar
			File nameFileOutput = new File (outPut);
			
			
			if (textoError.length() == 0) {
				GeneradorAssembler ga = new GeneradorAssembler(parser, this, analizadorLexico);
				codigo = ga.generarCodigoAssembler();
			}
			
			if (codigo != null && textoError.length() == 0) {
				String outputAssembler;
				outputAssembler = new File (MainClass.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
				outputAssembler = outputAssembler.replace("EjecutableCompilador.jar", "SalidaAssembler.asm");
				
//				File OutputAss = new File ("src/main/java/compilador/SalidaAssembler.asm"); // cambiar
				File OutputAss = new File (outputAssembler);
				
				FileWriter fwA = new FileWriter (OutputAss);
				fwA.write(codigo);
				fwA.close();
			}
			
			FileWriter fw = new FileWriter (nameFileOutput);
			
			fw.write(codigoFuente + "\n\n");
//			fw.write("TOKENS RECONOCIDOS\n\n" + textoToken.toString() + "\n\n");
//			fw.write("ESTRUCTURAS SINTACTICAS\n\n" + textoSintactica.toString() + "\n\n");
			fw.write("ADVERTENCIAS\n\n" + textoWarning.toString() + "\n\n");
			fw.write("ERRORES EN EL CODIGO\n\n" + textoError.toString() + "\n\n");
			fw.write("TABLA DE SIMBOLOS \n\n" + tablaSimbolos + "\n\n");
			fw.write("CODIGO INTERMEDIO \n\n" + codigoIntermedio(pila));
			
			fw.close();
			
			

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public String codigoIntermedio(List<String> pila) {
		String result = "";
		
		for (int x=0; x < pila.size(); x++) 
		  result += x + "  " + pila.get(x) + "\n";
	
		return result;
	}
	
	public static void main(String[] args) throws IOException {
		
//		MainClass menu = new MainClass();
//		menu.analizarCodigo("src/main/java/compilador/EjemploCodigo2.txt");
		
		if (args.length != 0) {
			MainClass menu = new MainClass();
			menu.analizarCodigo(args[0]);
		}
		
	}
	
	@Override
	public void tablaDeSimbolos(String tabla) {
		tablaSimbolos = tabla;
	}

	@Override
	public void error(int nroLinea, String error) {
		textoError.append("Linea " + nroLinea + ": Error: " + error + "\n");
	}

	@Override
	public void token(int nroLinea,String tipo ,String lexema) {
		if (tipo != null)
			textoToken.append("Linea " + nroLinea + ": " + tipo + ": " + lexema + "\n");
		else 
			textoToken.append("Linea " + nroLinea + ": " + lexema + "\n");
	}

	@Override
	public void warning(String warning) {
		textoWarning.append(warning + "\n");
	}

	@Override
	public void estructuraSintactica(int linea, String estructura) {
		textoSintactica.append("Linea " + linea + ": " + estructura + "\n");
	}

	@Override
	public void errorTipos(String error, String var1, String var2) {
		textoError.append("Error: " + error + " - Entre '" + var1 + "' y '" + var2 + "'\n");
		
	}
	
}
