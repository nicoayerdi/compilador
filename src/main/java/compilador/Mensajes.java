package compilador;

public interface Mensajes {

	public void tablaDeSimbolos(String tabla);

	public void error(int nroLinea, String mensaje);
	
	public void errorTipos(String mensaje, String var1, String var2);

	public void token(int nroLinea, String tipo, String lexema) ;
	
	public void warning(String string) ;
		
	public void estructuraSintactica(int linea, String estructura);
	
}
