.386
.model flat, stdcall
option casemap :none
include \masm32\include\windows.inc
include \masm32\include\kernel32.inc
include \masm32\include\user32.inc
includelib <c:\masm32\lib\kernel32.lib>
includelib \masm32\lib\user32.lib
.data
__cte1 DQ 5.0
_Joaco@sueldo DQ ? 
_sueldo DQ ? 
@aux2 DQ ? 
@aux1 DQ ? 
__cte2 DQ 2.0
_x DQ ? 
__cte3 DQ 9.0E307
_Martin@sueldo DQ ? 
error_overflow DB "Se produjo un overflow en una suma" , 0
error_overflow_mul DB "Se produjo un overflow en la multiplicacion" , 0
error_division DB "Se produjo una division por 0" , 0
error_titulo DB "ERROR" , 0
imprimir_titulo db "Mensaje" , 0
__cteceroDW DW 0 
__cteceroDQ DQ 0.0 
__ctemaxdoublepos DQ 1.7976931348623157E308 
.code
labelmayordeEdad: 
FLD __cte1
FADD _sueldo
FCOM __ctemaxdoublepos 
FSTSW AX 
SAHF 
JB label1
invoke MessageBox, NULL, addr error_overflow, addr error_titulo, MB_OK
invoke ExitProcess, 0 
label1: 
FSTP @aux1
FLD @aux1
FSTP _sueldo
RET 
start: 
FLD __cte3
FSTP _Joaco@sueldo
FLD _Joaco@sueldo
FMUL __cte2
FCOM __ctemaxdoublepos 
FSTSW AX 
SAHF 
JB label2
invoke MessageBox, NULL, addr error_overflow_mul, addr error_titulo, MB_OK
invoke ExitProcess, 0 
label2: 
FSTP @aux2
FLD @aux2
FSTP _x
invoke ExitProcess, 0
end start