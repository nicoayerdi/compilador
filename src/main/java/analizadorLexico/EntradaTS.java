package analizadorLexico;

import java.util.ArrayList;
import java.util.List;

public class EntradaTS {
	
	private Short id;
	private String lexema;
	private int contRef;
	private String tipo;
	private String uso;
	private String visibilidad; // Para atributos 
	private String ambito; // Para variables y metodos
	private String padre; // Para clases
	private List<String> listaAtributos; // Para clases
	private String nombreAssembler; // Nombre de variable que se utiliza en assembler
	
	public EntradaTS(Short id, String lexema) {
		this.id = id; 
		this.setLexema(lexema);
		contRef = 1;
		tipo = null;
		visibilidad = null;
		padre = null;
		uso = null;
		ambito = null;
		nombreAssembler = null;
		setListaAtributos(new ArrayList<String>());
	}
	
	public EntradaTS(String lexema, String uso) {
		this.id = null; 
		this.setLexema(lexema);
		contRef = 1;
		tipo = null;
		visibilidad = null;
		padre = null;
		this.setUso(uso);
		ambito = null;
		nombreAssembler = null;
		setListaAtributos(new ArrayList<String>());
	}

	public int getContRef() {
		return contRef;
	}

	public void setContRef(int contRef) {
		this.contRef = contRef;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public Short getId() {
		return this.id;
	}

	public void incrementarCont() {
		contRef++;

	}
	public void decContRef(){
		this.contRef--;
	}

	public String getLexema() {
		return lexema;
	}

	public void setLexema(String lexema) {
		this.lexema = lexema;
	}

	public String toString(){
		return "Lexema: " + lexema + " contRef: " + contRef + ", tipo: " + tipo + ", uso: " + uso + ", padre: " + padre + ", visibilidad: " + visibilidad + ", ambito: " + ambito + ", nombreAssembler: " + nombreAssembler;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUso() {
		return uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}

	public String getVisibilidad() {
		return visibilidad;
	}

	public void setVisibilidad(String visibilidad) {
		this.visibilidad = visibilidad;
	}

	public String getPadre() {
		return padre;
	}

	public void setPadre(String padre) {
		this.padre = padre;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	public List<String> getListaAtributos() {
		return listaAtributos;
	}

	public void setListaAtributos(List<String> listaAtributos) {
		this.listaAtributos = listaAtributos;
	}
	
	public void addAtributo(String atributo) {
		this.listaAtributos.add(atributo);
	}

	public String getNombreAssembler() {
		return nombreAssembler;
	}

	public void setNombreAssembler(String nombreAssembler) {
		this.nombreAssembler = nombreAssembler;
	}
	 
}
