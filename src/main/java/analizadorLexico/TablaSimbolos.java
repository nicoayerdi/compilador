package analizadorLexico;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class TablaSimbolos {

	private List <String> palabrasReservadas;
    private Hashtable <String,EntradaTS> lexemas;


    public TablaSimbolos(){
   	 	palabrasReservadas = new ArrayList<String>();
        lexemas = new Hashtable <String,EntradaTS>();
        palabrasReservadas.add("if");
        palabrasReservadas.add("else");
        palabrasReservadas.add("end_if");
        palabrasReservadas.add("print");
        palabrasReservadas.add("int");
        palabrasReservadas.add("begin");
        palabrasReservadas.add("end");
        palabrasReservadas.add("double");
        palabrasReservadas.add("while");
        palabrasReservadas.add("do");
        palabrasReservadas.add("class");
        palabrasReservadas.add("public");
        palabrasReservadas.add("private");
        palabrasReservadas.add("extends");
        palabrasReservadas.add("void");
        
    }
    
    public Set<String> getKeySet(){
    	return lexemas.keySet();
    }
    
    public Hashtable <String,EntradaTS> getTabla(){
    	return this.lexemas;
    }
	
    public boolean contieneLexema(String lexema) {
        return this.lexemas.containsKey(lexema);
    }

    public  EntradaTS getEntradaTS(String lexema) {
        return this.lexemas.get(lexema);
    }

    public boolean esPalabraReservada(String valor) {
        return this.palabrasReservadas.contains(valor);
    }
    
    public void addETS(String lexema, EntradaTS entrada) {
        this.lexemas.put(lexema, entrada);
    }
    
    public void removeETS(String lexema){
    	this.lexemas.remove(lexema);
    }
        
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	for (String s: lexemas.keySet()) 
    		sb.append("LEXEMA: " + s + " --> " + lexemas.get(s).toString() + "\n");
    	sb.append("\nPalabras Reservadas: \n");
    	sb.append(palabrasReservadas.toString());
    	return sb.toString();
    }
   
    public boolean ultimaReferencia (String lexema) {	
    	if (lexemas.get(lexema) != null && lexemas.get(lexema).getContRef() == 1) 
    		return true;
    	return false;
    	
    }
    
    public void disminuirReferencia (String lexema) {
    	lexemas.get(lexema).decContRef();
    }
    
    public void aumentarReferencia (String lexema) {
    	lexemas.get(lexema).incrementarCont();
    }
    
	public boolean declarada(String lexema){
    	EntradaTS aux = this.lexemas.get(lexema);
    	if (aux!=null && aux.getContRef() > 1) 
    		return true;
    	return false;
    }
    
    
    
}
