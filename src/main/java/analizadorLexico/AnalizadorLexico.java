package analizadorLexico;

import accionesSemanticas.AS1;
import accionesSemanticas.AS10;
import accionesSemanticas.AS11;
import accionesSemanticas.AS12;
import accionesSemanticas.AS2;
import accionesSemanticas.AS3;
import accionesSemanticas.AS4;
import accionesSemanticas.AS5;
import accionesSemanticas.AS6;
import accionesSemanticas.AS7;
import accionesSemanticas.AS8;
import accionesSemanticas.AS9;
import accionesSemanticas.AccSemantica;
import compilador.Mensajes;

public class AnalizadorLexico {

	public static final int FIN = 265;  // Estado final	
	private String codigoFuente;        // Codigo fuente del archivo de entrada
	private int estado;                 // Estado actual
	private int posicion;				// Posicion en el codigo fuente
	private int nroLinea;               // Numero de linea en el codigo fuente
	private AccSemantica as1, as2, as3, as4, as5, as6, as7, as8, as9, as10, as11, as12; 	// Acciones semanticas
	private Token token;				// Token actual
	private boolean endOfFile;			// Booleano que indica si se llego al final del archivo
	private TablaSimbolos tablaSimbolos;// Tabla de simbolos que contiene los tokens
	private Mensajes msj;				// Tipos de mensajes
	private boolean fin;
	
	private int [][] matrizTE ={		// Matriz de transicion de estados
	        //l  d  _  .  D  +  -  *  /  (  )  :  ;  ,  =  <  >  #  \n ' ' { } otro
	        //0  1  2  3  4  5  6  7  8  9 10  11 12 13 14 15 16 17 18 19 20 21 22
	        { 1, 2,-1, 4, 1,-1,-1,-1,-1,-1,-1, 8,-1,-1, 11,9,10,12, 0, 0,13,-1,-1}, //0
	        {1, 1, 1 ,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //1
	        {-1, 2,-1, 3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //2
	        {-1, 3,-1,-1, 5,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //3
	        {-1, 3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //4
	        {-1, 7,-1,-1,-1, 6, 6,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //5
	        {-1, 7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //6
	        {-1, 7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //7
	        {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //8
	        {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //9
	        {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //10
	        {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},  //11
	        {12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12, 0,12,12,12,12},  //12
	        {13,13,13,13,13,13,13,13,13,13,-1,13,13,13,13,13,13,13,13,13,13,-1,13},  //13
			};
	
	private AccSemantica [][] matrizAS;
	
	public AnalizadorLexico (String codigoFuente, Mensajes m) {
		tablaSimbolos = new TablaSimbolos();
		msj = m;
		this.codigoFuente = codigoFuente;
		this.nroLinea = 1;
		endOfFile = false;
		fin = false;
		estado = 0;
		posicion = 0;
		inicializarAS();
	}
	
	private void inicializarAS() {
		
		as1 = new AS1();
		as2 = new AS2();
		as3 = new AS3(msj, this);
		as4 = new AS4(msj, this);
		as5 = new AS5(msj, this);
		as6 = new AS6();
		as7 = new AS7();
		as8 = new AS8(tablaSimbolos, msj, this);
		as9 = new AS9(msj, this, tablaSimbolos);
		as10 = new AS10(tablaSimbolos, this, msj);
		as11 = new AS11(msj, this, tablaSimbolos);
		as12 = new AS12(msj, this);
		
		matrizAS = new AccSemantica[14][23];
		
		// Estado 0
		matrizAS[0][0 ] =as1 ;
        matrizAS[0][1 ] =as1 ;
        matrizAS[0][2 ] =as12;
        matrizAS[0][3 ] =as1 ;
        matrizAS[0][4 ] =as1 ;
        matrizAS[0][5 ] =as3 ;
        matrizAS[0][6 ] =as3 ;
        matrizAS[0][7 ] =as3 ;
        matrizAS[0][8 ] =as3 ;
        matrizAS[0][9 ] =as3 ;
        matrizAS[0][10] =as3 ;
        matrizAS[0][11] =as1 ;
        matrizAS[0][12] =as3 ;
        matrizAS[0][13] =as3 ;
        matrizAS[0][14] =as1 ;
        matrizAS[0][15] =as1 ;
        matrizAS[0][16] =as1 ;
        matrizAS[0][17] =as6 ;
        matrizAS[0][18] =as6 ;
        matrizAS[0][19] =as6;
        matrizAS[0][20] =as7;
        matrizAS[0][21] =as12;
        matrizAS[0][22] =as12;
        
        // Estado 1
        matrizAS[1][0 ] =as2 ;
        matrizAS[1][1 ] =as2 ;
        matrizAS[1][2 ] =as2 ;
        matrizAS[1][3 ] =as8 ;
        matrizAS[1][4 ] =as2 ;
        matrizAS[1][5 ] =as8 ;
        matrizAS[1][6 ] =as8 ;
        matrizAS[1][7 ] =as8 ;
        matrizAS[1][8 ] =as8 ;
        matrizAS[1][9 ] =as8 ;
        matrizAS[1][10] =as8 ;
        matrizAS[1][11] =as8 ;
        matrizAS[1][12] =as8 ;
        matrizAS[1][13] =as8 ;
        matrizAS[1][14] =as8 ;
        matrizAS[1][15] =as8 ;
        matrizAS[1][16] =as8 ;
        matrizAS[1][17] =as8 ;
        matrizAS[1][18] =as8 ;
        matrizAS[1][19] =as8 ;
        matrizAS[1][20] =as8 ;
        matrizAS[1][21] =as8 ;
        matrizAS[1][22] =as12;
        
        // Estado 2
        matrizAS[2][0 ] =as10;
        matrizAS[2][1 ] =as2;
        matrizAS[2][2 ] =as10;
        matrizAS[2][3 ] =as2;
        matrizAS[2][4 ] =as10;
        matrizAS[2][5 ] =as10;
        matrizAS[2][6 ] =as10;
        matrizAS[2][7 ] =as10;
        matrizAS[2][8 ] =as10;
        matrizAS[2][9 ] =as10;
        matrizAS[2][10] =as10;
        matrizAS[2][11] =as10;
        matrizAS[2][12] =as10;
        matrizAS[2][13] =as10;
        matrizAS[2][14] =as10;
        matrizAS[2][15] =as10;
        matrizAS[2][16] =as10;
        matrizAS[2][17] =as10;
        matrizAS[2][18] =as10;
        matrizAS[2][19] =as10;
        matrizAS[2][20] =as10;
        matrizAS[2][21] =as10;
        matrizAS[2][22] =as12;

        // Estado 3
        matrizAS[3][0 ] =as11;
        matrizAS[3][1 ] =as2;
        matrizAS[3][2 ] =as11;
        matrizAS[3][3 ] =as11;
        matrizAS[3][4 ] =as2;
        matrizAS[3][5 ] =as11;
        matrizAS[3][6 ] =as11;
        matrizAS[3][7 ] =as11;
        matrizAS[3][8 ] =as11;
        matrizAS[3][9 ] =as11;
        matrizAS[3][10] =as11;
        matrizAS[3][11] =as11;
        matrizAS[3][12] =as11;
        matrizAS[3][13] =as11;
        matrizAS[3][14] =as11;
        matrizAS[3][15] =as11;
        matrizAS[3][16] =as11;
        matrizAS[3][17] =as11;
        matrizAS[3][18] =as11;
        matrizAS[3][19] =as11;
        matrizAS[3][20] =as11;
        matrizAS[3][21] =as11;
        matrizAS[3][22] =as12;
        
        //ESTADO 4
        matrizAS[4][0 ] =as5;
        matrizAS[4][1 ] =as2;
        matrizAS[4][2 ] =as5;
        matrizAS[4][3 ] =as5;
        matrizAS[4][4 ] =as5;
        matrizAS[4][5 ] =as5;
        matrizAS[4][6 ] =as5;
        matrizAS[4][7 ] =as5;
        matrizAS[4][8 ] =as5;
        matrizAS[4][9 ] =as5;
        matrizAS[4][10] =as5;
        matrizAS[4][11] =as5;
        matrizAS[4][12] =as5;
        matrizAS[4][13] =as5;
        matrizAS[4][14] =as5;
        matrizAS[4][15] =as5;
        matrizAS[4][16] =as5;
        matrizAS[4][17] =as5;
        matrizAS[4][18] =as5;
        matrizAS[4][19] =as5;
        matrizAS[4][20] =as5;
        matrizAS[4][21] =as5;
        matrizAS[4][22] =as5;

        //ESTADO 5
        matrizAS[5][0 ] =as12;
        matrizAS[5][1 ] =as2;
        matrizAS[5][2 ] =as12;
        matrizAS[5][3 ] =as12;
        matrizAS[5][4 ] =as12;
        matrizAS[5][5 ] =as2;
        matrizAS[5][6 ] =as2;
        matrizAS[5][7 ] =as12;
        matrizAS[5][8 ] =as12;
        matrizAS[5][9 ] =as12;
        matrizAS[5][10] =as12;
        matrizAS[5][11] =as12;
        matrizAS[5][12] =as12;
        matrizAS[5][13] =as12;
        matrizAS[5][14] =as12;
        matrizAS[5][15] =as12;
        matrizAS[5][16] =as12;
        matrizAS[5][17] =as12;
        matrizAS[5][18] =as12;
        matrizAS[5][19] =as12;
        matrizAS[5][20] =as12;
        matrizAS[5][21] =as12;
        matrizAS[5][22] =as12;

        //estado 6
        matrizAS[6][0 ] =as12;
        matrizAS[6][1 ] =as2;
        matrizAS[6][2 ] =as12;
        matrizAS[6][3 ] =as12;
        matrizAS[6][4 ] =as12;
        matrizAS[6][5 ] =as12;
        matrizAS[6][6 ] =as12;
        matrizAS[6][7 ] =as12;
        matrizAS[6][8 ] =as12;
        matrizAS[6][9 ] =as12;
        matrizAS[6][10] =as12;
        matrizAS[6][11] =as12;
        matrizAS[6][12] =as12;
        matrizAS[6][13] =as12;
        matrizAS[6][14] =as12;
        matrizAS[6][15] =as12;
        matrizAS[6][16] =as12;
        matrizAS[6][17] =as12;
        matrizAS[6][18] =as12;
        matrizAS[6][19] =as12;
        matrizAS[6][20] =as12;
        matrizAS[6][21] =as12;
        matrizAS[6][22] =as12;
        
        //ESTADO 7
        matrizAS[7][0 ] =as11;
        matrizAS[7][1 ] =as2;
        matrizAS[7][2 ] =as11;
        matrizAS[7][3 ] =as11;
        matrizAS[7][4 ] =as11;
        matrizAS[7][5 ] =as11;
        matrizAS[7][6 ] =as11;
        matrizAS[7][7 ] =as11;
        matrizAS[7][8 ] =as11;
        matrizAS[7][9 ] =as11;
        matrizAS[7][10] =as11;
        matrizAS[7][11] =as11;
        matrizAS[7][12] =as11;
        matrizAS[7][13] =as11;
        matrizAS[7][14] =as11;
        matrizAS[7][15] =as11;
        matrizAS[7][16] =as11;
        matrizAS[7][17] =as11;
        matrizAS[7][18] =as11;
        matrizAS[7][19] =as11;
        matrizAS[7][20] =as11;
        matrizAS[7][21] =as11;
        matrizAS[7][22] =as12;
        
        //ESTADO 8
        matrizAS[8][0 ] =as12;
        matrizAS[8][1 ] =as12;
        matrizAS[8][2 ] =as12;
        matrizAS[8][3 ] =as12;
        matrizAS[8][4 ] =as12;
        matrizAS[8][5 ] =as12;
        matrizAS[8][6 ] =as12;
        matrizAS[8][7 ] =as12;
        matrizAS[8][8 ] =as12;
        matrizAS[8][9 ] =as12;
        matrizAS[8][10] =as12;
        matrizAS[8][11] =as12;
        matrizAS[8][12] =as12;
        matrizAS[8][13] =as12;
        matrizAS[8][14] =as4;
        matrizAS[8][15] =as12;
        matrizAS[8][16] =as12;
        matrizAS[8][17] =as12;
        matrizAS[8][18] =as12;
        matrizAS[8][19] =as12;
        matrizAS[8][20] =as12;
        matrizAS[8][21] =as12;
        matrizAS[8][22] =as12;
        
        //Estado 9
        matrizAS[9][0 ] =as5;
        matrizAS[9][1 ] =as5;
        matrizAS[9][2 ] =as5 ;
        matrizAS[9][3 ] =as5;
        matrizAS[9][4 ] =as5;
        matrizAS[9][5 ] =as5;
        matrizAS[9][6 ] =as5;
        matrizAS[9][7 ] =as5;
        matrizAS[9][8 ] =as5;
        matrizAS[9][9 ] =as5;
        matrizAS[9][10] =as5;
        matrizAS[9][11] =as5;
        matrizAS[9][12] =as5;
        matrizAS[9][13] =as5;
        matrizAS[9][14] =as4;
        matrizAS[9][15] =as5;
        matrizAS[9][16] =as4;
        matrizAS[9][17] =as5;
        matrizAS[9][18] =as5;
        matrizAS[9][19] =as5;
        matrizAS[9][20] =as5;
        matrizAS[9][21] =as5;
        matrizAS[9][22] =as12;

        //estado 10
        matrizAS[10][0 ] =as5;
        matrizAS[10][1 ] =as5;
        matrizAS[10][2 ] =as5;
        matrizAS[10][3 ] =as5;
        matrizAS[10][4 ] =as5;
        matrizAS[10][5 ] =as5;
        matrizAS[10][6 ] =as5;
        matrizAS[10][7 ] =as5;
        matrizAS[10][8 ] =as5;
        matrizAS[10][9 ] =as5;
        matrizAS[10][10] =as5;
        matrizAS[10][11] =as5;
        matrizAS[10][12] =as5;
        matrizAS[10][13] =as5;
        matrizAS[10][14] =as4;
        matrizAS[10][15] =as5;
        matrizAS[10][16] =as5;
        matrizAS[10][17] =as5;
        matrizAS[10][18] =as5;
        matrizAS[10][19] =as5;
        matrizAS[10][20] =as5;
        matrizAS[10][21] =as5;
        matrizAS[10][22] =as12;
        
        //estado 11
        matrizAS[11][0 ] =as12;
        matrizAS[11][1 ] =as12;
        matrizAS[11][2 ] =as12;
        matrizAS[11][3 ] =as12;
        matrizAS[11][4 ] =as12;
        matrizAS[11][5 ] =as12;
        matrizAS[11][6 ] =as12;
        matrizAS[11][7 ] =as12;
        matrizAS[11][8 ] =as12;
        matrizAS[11][9 ] =as12;
        matrizAS[11][10] =as12;
        matrizAS[11][11] =as12;
        matrizAS[11][12] =as12;
        matrizAS[11][13] =as12;
        matrizAS[11][14] =as4;
        matrizAS[11][15] =as12;
        matrizAS[11][16] =as12;
        matrizAS[11][17] =as12;
        matrizAS[11][18] =as12;
        matrizAS[11][19] =as12;
        matrizAS[11][20] =as12;
        matrizAS[11][21] =as12;
        matrizAS[11][22] =as12;
        
        //estado 12
        matrizAS[12][0 ] =as6;
        matrizAS[12][1 ] =as6;
        matrizAS[12][2 ] =as6;
        matrizAS[12][3 ] =as6;
        matrizAS[12][4 ] =as6;
        matrizAS[12][5 ] =as6;
        matrizAS[12][6 ] =as6;
        matrizAS[12][7 ] =as6;
        matrizAS[12][8 ] =as6;
        matrizAS[12][9 ] =as6;
        matrizAS[12][10] =as6;
        matrizAS[12][11] =as6;
        matrizAS[12][12] =as6;
        matrizAS[12][13] =as6;
        matrizAS[12][14] =as6;
        matrizAS[12][15] =as6;
        matrizAS[12][16] =as6;
        matrizAS[12][17] =as6;
        matrizAS[12][18] =as6;
        matrizAS[12][19] =as6;
        matrizAS[12][20] =as6;
        matrizAS[12][21] =as6;
        matrizAS[12][22] =as6;

        //estado 13
        matrizAS[13][0 ] =as2;
        matrizAS[13][1 ] =as2;
        matrizAS[13][2 ] =as2;
        matrizAS[13][3 ] =as2;
        matrizAS[13][4 ] =as2;
        matrizAS[13][5 ] =as2;
        matrizAS[13][6 ] =as2;
        matrizAS[13][7 ] =as2;
        matrizAS[13][8 ] =as2;
        matrizAS[13][9 ] =as2;
        matrizAS[13][10] =as12;
        matrizAS[13][11] =as2;
        matrizAS[13][12] =as2;
        matrizAS[13][13] =as2;
        matrizAS[13][14] =as2;
        matrizAS[13][15] =as2;
        matrizAS[13][16] =as2;
        matrizAS[13][17] =as2;
        matrizAS[13][18] =as6;
        matrizAS[13][19] =as2;
        matrizAS[13][20] =as2;
        matrizAS[13][21] =as9;
        matrizAS[13][22] =as2;
       
	}
	
	
	public int yylex(){ // Devuelve el ID del token pedido por el parser
    	this.estado = 0;
    	this.token = new Token();
        
        while (estado != -1 && !endOfFile){
            char caracter = codigoFuente.charAt(posicion);                      	
            int simbolo = getColumna(caracter);   
       
            token = (matrizAS[estado][simbolo]).ejecutar(token, caracter);     
            
            if (!token.consumioCaracter())                                      
                posicion--;                                                    
            if (caracter == '\n' && token.consumioCaracter())                  
                nroLinea++;                                                    
            posicion++;         
            estado = matrizTE[estado][simbolo];     
            
        }
        
        if (endOfFile && !fin){			// Cuando llegamos al final del archivo
            msj.tablaDeSimbolos(tablaSimbolos.toString());
            fin = true;
            return FIN;					// Primero devolvemos el "FIN"
        }

        if (endOfFile && fin)           // Cuando llegamos al final del archivo
            return 0;                   // Luego de devolver el "FIN" devolvemos el "0"

        return token.getId().intValue();
    }
		
	public void imprimirTabla(){
    	msj.tablaDeSimbolos(tablaSimbolos.toString());
    }
	
    public void releerCaracter(){		// Retrocede la posicion para releer un caracter
        posicion--;
    }
    
    public int getNroLinea(){			// Devuelve el numero de linea actual
        return this.nroLinea;
    }

    public Token getToken(){			// Devuelve el Token actual
        return token;
    }
    
    public TablaSimbolos getTablaDeSimbolos(){	// Devuelve la tabla de simbolos
        return tablaSimbolos;
    }
    
    public String getMensaje(int nro){ 
    	switch(nro){
    
    	//ERRORES LEXICOS
    	case 2: return "Caracter no identificado";
    	case 3: return "Construccion de token erroneo";
    	case 4: return "Constante Entera Fuera de Rango Permitido";
    	case 5: return "Constante Double Fuera de Rango Permitido";

        //ERRORES SINTACTICOS
        case 7: return "No se encontro el fin de archivo";
        case 8: return "Falta el bloque de sentencias ejecutables";    
        case 9: return "Falta el bloque de sentencias declarativas";
        case 10: return "Se esperaba un ';'";
        case 11: return "Falta el tipo de la declaracion";
        case 12: return "Sentencia declarativa incorrecta";
        case 13: return "Falta el identificador de la asignacion";
        case 14: return "Falta el identificador de la asignacion y se esperaba un ';'";
        case 15: return "Bloque de sentencias sin inicializar falta BEGIN";    
        case 16: return "Bloque de sentencias sin finalizar falta END";
        case 19: return "Parametro del PRINT incorrecto";
        case 20: return "Falta palabra reservada 'PRINT'";
        case 21: return "Sentencia incorrecta";
        case 22: return "Falta abrir parentesis '(' ";
        case 23: return "Falta cerrar parentesis ')' ";
        case 24: return "Falta nombre del objeto al que se quiere acceder";
        case 25: return "Falta nombre del atributo al que se quiere acceder ";
        case 26: return "Falta la visibilidad del atributo";
        case 27: return "Falta cierre del if: END_IF";
        case 28: return "No puede haber mas de una sentencia que no se encuentr entre begin y end";
        case 29: return "Error en la expresion";
        
        
        // ESTRUCTURAS SINTACTICAS
        case 31: return "Sentencia declarativa";
        case 32: return "Sentencia de asignacion";
        case 33: return "Sentencia de seleccion";
        case 34: return "Sentencia de iteracion";
        case 35: return "Sentencia de impresion de caracteres";
        case 36: return "Bloque de sentencias";
        case 37: return "Sentencia declarativa: CLASE"; 
        case 38: return "Sentencia declarativa: metodos de CLASE";
        case 39: return "Sentencia de asignacion: Objetos";
        case 40: return "Sentencia de ejecucion de metodos";
        case 41: return "Fin de programa";
    	
   	
    	// ERRORES SEMANTICOS
    	case 50: return "Variable Redeclarada";
    	case 51: return "Clase Redeclarada";
    	case 52: return "Clase No Declarada";
    	case 53: return "Metodo Redeclarado";
    	case 54: return "Atributo Redeclarado";
    	case 55: return "Atributo/Metodo No Declarado";
    	case 56: return "Atributo/Metodo No Declarado en la Clase";
    	case 57: return "No se tiene acceso a Atributo Privado";
    	case 58: return "No se tiene acceso a Metodo Privado";
    	case 59: return "No se tiene acceso a Objetos dentro de la declaración de Clases";
    	case 60: return "No acceder a nombres de variables en llamadas a metodos";
    	case 61: return "No acceder a nombres de metodos en acceso a variables";
    	case 62: return "Variable No Declarada";
    	case 63: return "Variable fuera de ambito";
    	case 64: return "Metodo fuera de ambito";
    	case 65: return "Metodo No Declarado"; 
    	case 66: return "Tipos Incompatibles";
    	
    	}
    	return null;
    }
	
	public int getColumna(int caracter) { // Obtiene la columna en la que esta cada caracter en la matriz de estados 
	
		if ((caracter >= 65 && caracter <= 90) && (caracter != 'D')) // Mayusculas
			return 0;
		if ((caracter >= 97 && caracter <= 122) && (caracter != 'd')) // Minusculas
			return 0;
		if ((caracter >= 48) && (caracter <= 57)) // Retorna columna de digitos
			return 1;  
		if (caracter == '_')
			return 2;
		if (caracter == '.')
			return 3;
		if (caracter == 'D' || caracter == 'd')
			return 4;
		if (caracter == '+')
			return 5;
		if (caracter == '-')
			return 6;
		if (caracter == '*')
			return 7;
		if (caracter == '/')
			return 8;
		if (caracter == '(')
			return 9;
		if (caracter == ')')
			return 10;
		if (caracter == ':')
			return 11;
		if (caracter == ';')
			return 12;
		if (caracter == ',')
			return 13;
		if (caracter == '=')
			return 14;
		if (caracter == '<')
			return 15;
		if (caracter == '>')  
			return 16;
		if (caracter == '#')
			return 17;
		if (caracter == '\n' )
			return 18;
		if ((caracter == 32) || (caracter == 9)) // ESPACIO BLANCO O TABULACION O CARRY
			return 19;
		if (caracter == '{' ) 
			return 20;
		if (caracter == '}' )
			return 21;
		if (caracter == 255) {
			endOfFile = true;
			return 18;  //FIN DE ARCHIVO
		}
		return 22;
	}
	
}
