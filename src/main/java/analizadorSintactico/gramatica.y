%{
package analizadorSintactico;
import analizadorLexico.EntradaTS;
import analizadorLexico.Token;
import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import compilador.Mensajes;
import java.util.ArrayList;
import java.util.Stack;
import java.util.List;
import java.util.HashMap;


%}

%token IF ELSE END_IF PRINT INT BEGIN END DOUBLE FIN WHILE DO CLASS PUBLIC PRIVATE ID CTEENTERO CTEDOUBLE COMODIN COMPARADOR ASIGNACION CADENACARACTERES VOID EXTENDS


%%

program: declaraciones bloque_sentencias FIN
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(41));		
	}	
	| error BEGIN sentencias END FIN 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(9));
	} 						
	| declaraciones error FIN 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(8));
	}											
	| declaraciones BEGIN sentencias END
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(7));
	}				
;

declaraciones: declaraciones declaracion
	| declaracion
;

declaracion: variable 					
	| clase
;

variable: tipo list_var ';'
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(31));
		List<Token> aux = new ArrayList<>();

		for (Token e: ((List<Token>) $2.obj)){
			if (tabla.declarada(e.getLexema())){
				e.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(50)+": " + e.getLexema());
				//variable redeclarada
			}
			else {	
				String tipo = ((String) $1.obj);
				aux.add(e);																			
				if (tipo.equals("int") || tipo.equals("double")){
					e.getETS().setTipo(tipo);
					e.getETS().setAmbito(ambito);
					e.getETS().setUso("Nombre de Variable");
				} else {
					if (tabla.declarada(tipo)){
						e.getETS().setTipo(tipo);
						e.getETS().setAmbito(ambito);
						if (ambito.equals("Main"))
							e.getETS().setUso("Nombre de Objeto");							
						while (tipo != null){
							for (String l: tabla.getEntradaTS(tipo).getListaAtributos()){
								tabla.addETS(e.getLexema() + "@" + l, newEntradaTS(l));
							}	
							tipo = tabla.getEntradaTS(tipo).getPadre(); 
						}
					} else 
						tabla.removeETS(e.getLexema());
				}
			}	
		}
	
		$$.obj = ((List<Token>) aux);
	}
	| tipo error ';'
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(12));
		$$.obj = new ArrayList<Token>();
	}
	| tipo list_var
	{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
		$$.obj = new ArrayList<Token>();
	}
;

clase: CLASS encabezado_clase BEGIN sentencias_class END
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(37));
		String lexema = (String) $2.obj;
		tabla.getEntradaTS(lexema).setListaAtributos((List<String>) $4.obj);
		ambito = "Main";
	}
;

encabezado_clase: ID
	{
		Token token = (Token) $1.obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema()))
			{
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(51)+": " + token.getLexema());
				//variable redeclarada
			}
			else {																																																																
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
			}
		$$.obj = token.getLexema();
	}
	| ID EXTENDS ID
	{
		Token token= (Token) $1.obj;
		Token token2 = (Token) $3.obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema())){
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(51)+": " + token.getLexema());
				//variable redeclarada
				if (token.getLexema().equals(token2.getLexema()))
					token2.getETS().decContRef();
				else
					if (tabla.ultimaReferencia(token2.getLexema()))
						tabla.removeETS(token2.getLexema());
					else
						token2.getETS().decContRef();
			}
			else {		
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
				if (tabla.declarada(token2.getLexema())){
					token2.getETS().decContRef();
					token.getETS().setPadre(token2.getLexema());
				} else{
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token2.getLexema());
					tabla.removeETS(token2.getLexema());
				}
			}
		$$.obj = token.getLexema();
	}
;

sentencias_class: sentencias_class class_body
	{
		List<String> aux = (List<String>) $1.obj;
		aux.addAll((List<String>) $2.obj);
		$$.obj = aux;
	}
	| class_body
	{
		$$.obj = $1.obj;
	}
;

class_body: clase_atributos
	{
		$$.obj = $1.obj;
	}
	| clase_metodos
	{
		$$.obj = $1.obj;
	}
;

clase_atributos: visibilidad variable
	{
		List<String> lexemas = new ArrayList<>();
		for (Token e: ((List<Token>) $2.obj)){
			e.getETS().setUso("Nombre de Atributo");
			e.getETS().setVisibilidad((((Token) $1.obj).getLexema()).toLowerCase());
			lexemas.add(e.getLexema());
		}
		$$.obj = lexemas;
	}	
	| variable
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(26));
		List<String> lexemas = new ArrayList<>();
		$$.obj = lexemas;
	}			
;

clase_metodos: encabezado_metodo '(' ')' bloque_sentencias
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(38));
		List<String> lexemas = new ArrayList<>();
		if ($1.obj != null){
			lexemas.add((String) $1.obj);
			polaca.add("FM");
			cantMetodos++;
		}
		$$.obj = lexemas;
	}
	| encabezado_metodo '(' bloque_sentencias
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23));
	}
	| encabezado_metodo ')' bloque_sentencias	
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22));
	}	
;

encabezado_metodo: visibilidad VOID ID
{
	Token token = (Token) $3.obj;
	$$.obj = null;
	if (tabla.declarada(token.getLexema())){
		token.getETS().decContRef();
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(53)+": " + token.getLexema());
	} else {
		token.getETS().setTipo((((Token) $2.obj).getLexema()).toLowerCase());																								
		token.getETS().setUso("Nombre de Metodo");	
		token.getETS().setVisibilidad((((Token) $1.obj).getLexema()).toLowerCase());
		token.getETS().setAmbito(ambito);

		polaca.add(token.getLexema());
		polaca.add("IM");

		$$.obj = token.getLexema();
	}
}
;
visibilidad: PUBLIC
	| PRIVATE
;

list_var: list_var ',' ID	
	{
		List<Token> tokens = (List<Token>) $1.obj;
		Token token = (Token) $3.obj;
		tokens.add(token);
		$$.obj = tokens;
	}					
	| ID
	{
		List<Token> tokens = new ArrayList<Token>();
		Token token = (Token)$1.obj;
		tokens.add(token);
		$$.obj = tokens;
	}
;

tipo: INT
	{
		$$.obj = ((Token) $1.obj).getLexema().toLowerCase();
	}
	| DOUBLE
	{
		$$.obj = ((Token) $1.obj).getLexema().toLowerCase();
	}
	| ID
	{
		Token token = (Token) $1.obj;
		if (!tabla.declarada(token.getLexema())){
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
		$$.obj = token.getLexema();
	}
;

sentencias: sentencias sentencia  
	| sentencia
;
	 
sentencia: asignacion 
    | metodo 
	{
		String objeto = (String) $1.obj;
		if (objeto != null){
			polaca.add(objeto);
			polaca.add("LM");
		}
	}
	| seleccion 
	| iteracion 
	| print
	| error ';'
	{
	  mensajes.error(analizador.getNroLinea(),analizador.getMensaje(21));
	}
;

objeto: ID '.' ID
	{
		Token tokenObj = (Token) $1.obj;
		Token tokenAtr = (Token) $3.obj;
		$$.obj = null;
		if (ambito.equals("Main")){
			if (!tabla.declarada(tokenObj.getLexema())){
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + tokenObj.getLexema());
				tabla.removeETS(tokenObj.getLexema());
				if (tabla.ultimaReferencia(tokenAtr.getLexema()))
					tabla.removeETS(tokenAtr.getLexema());
				else
					tabla.disminuirReferencia(tokenAtr.getLexema());
			} else {
				if (!tabla.declarada(tokenAtr.getLexema())){
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(55)+": " + tokenAtr.getLexema());
					tabla.removeETS(tokenAtr.getLexema());
				} else {
					String lexema = tokenObj.getLexema() + "@" + tokenAtr.getLexema();
					if (!tabla.contieneLexema(lexema))
						mensajes.error(analizador.getNroLinea(), analizador.getMensaje(56)+": " + tokenObj.getLexema());
					else
						$$.obj = (String) lexema;
				}
			}
		}
		else {
			mensajes.error(analizador.getNroLinea(), analizador.getMensaje(59));
			if (tabla.declarada(tokenObj.getLexema()))
				tabla.disminuirReferencia(tokenObj.getLexema());
			else 
				tabla.removeETS(tokenObj.getLexema());
			if (tabla.declarada(tokenAtr.getLexema()))
				tabla.disminuirReferencia(tokenAtr.getLexema());
			else 
				tabla.removeETS(tokenAtr.getLexema());
		}	
	}
	| error '.' ID
	{
		String atr = ((Token) $3.obj).getLexema();
		$$.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(24));
		if (tabla.declarada(atr))
			tabla.disminuirReferencia(atr);
		else 
			tabla.removeETS(atr);
	}			
	| ID '.' error
	{
		String obj = ((Token) $1.obj).getLexema();
		$$.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(25));
		if (tabla.declarada(obj))
			tabla.disminuirReferencia(obj);
		else 
			tabla.removeETS(obj);
	}
;

objeto_var: objeto
	{
		String objeto = (String) $1.obj;
		$$.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Atributo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(61)+": " + aux[1]);
				$$.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(57)+": " + aux[1]);
					$$.obj = null;
				}
			}
		}
	}
;

metodo: objeto '(' ')' ';'
	{
		String objeto = (String) $1.obj;
		$$.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Metodo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(60)+": " + aux[1]);
				$$.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(58)+": " + aux[1]);
					$$.obj = null;
				}
			}
		}
		
	}
	| ID '(' ')' ';'
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(40));
		Token token = (Token) $1.obj;
		$$.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso();
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Metodo")){
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							$$.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(58)+": " + token.getLexema());
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema());	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema());
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(60)+": " + token.getLexema());
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema());
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
	}
	| objeto '(' ')' error
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(10));
	}
	| objeto '(' ';' 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23));
	}
	| objeto ')' ';' 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22));
	}
; 

id: ID
	{
		Token token = (Token) $1.obj;
		$$.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso(); 
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Variable")) {
					if (ambitoLocal.equals(ambito))
						$$.obj = (String) token.getLexema();
				} else if (uso.equals("Nombre de Atributo")) {
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							$$.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(57)+": " + token.getLexema());
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema());	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema());
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(61)+": " + token.getLexema());
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema());
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
	}
;

asignacion: id ASIGNACION expresion ';'			
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(32));
		polaca.addAll((ArrayList<String>) $3.obj);
		polaca.add((String) $1.obj);
		polaca.add(":=");
	}
	| objeto_var ASIGNACION expresion ';'
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(39));
		polaca.addAll((ArrayList<String>) $3.obj);
		polaca.add((String) $1.obj);
		polaca.add(":=");
	} 
	| id ASIGNACION expresion  
	{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
	}          
	| objeto_var ASIGNACION expresion
	{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
	} 
	| ASIGNACION expresion ';'
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(13));
	}			
	| ASIGNACION expresion
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(14));
	}
;

seleccion: seleccion_simple ELSE bloque END_IF		
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		if(!auxiliar.isEmpty())
			auxiliar.pop();
		int nroPilaInicial = posiciones.pop();
		polaca.remove(nroPilaInicial);
		polaca.add(nroPilaInicial, String.valueOf(polaca.size()+1));
	}
	| seleccion_simple END_IF
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		polaca.remove(polaca.size()-1);
		posiciones.pop();
		polaca.remove(polaca.size()-1);
		int pos = auxiliar.pop();
		int val = Integer.valueOf(polaca.get(pos));
		polaca.remove(pos);
		polaca.add(pos, String.valueOf(val-2));
	}
	| seleccion_simple ELSE bloque error 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27));
	}
	| seleccion_simple error
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27));
	}
;

seleccion_simple: IF condicion_if bloque
	{
		int nroPilaInicial = posiciones.pop();
		auxiliar.push(nroPilaInicial);
		polaca.remove(nroPilaInicial);
		polaca.add(nroPilaInicial, String.valueOf(polaca.size()+3));
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BI");
	}
;

condicion_if: condicion
	{
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BF");
	}
;

condicion: '(' expresion COMPARADOR expresion ')'
	{
		polaca.addAll((ArrayList<String>) $2.obj);
		polaca.addAll((ArrayList<String>) $4.obj);
		polaca.add(((Token) $3.obj).getLexema());
	}			
;

condicion_while: condicion
	{
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BF");
	}
;

bloque_sentencias: BEGIN sentencias END	
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(36));
	}	
	| error sentencias END
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15));
	}		
	| BEGIN sentencias error	
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16));
	}
;	

bloque: sentencia	
	| BEGIN sentencias END
	| BEGIN sentencias error 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16));
	}
	| END 
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15));
	}
;

iteracion: while condicion_while DO bloque
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(34));
		int nrobifurcacion = posiciones.pop();
		polaca.remove(nrobifurcacion);
		polaca.add(nrobifurcacion, String.valueOf(polaca.size()+3));
		int nroPilaInicial = posiciones.pop();
		polaca.add(String.valueOf(nroPilaInicial));
		polaca.add("BI");
	}
;

while: WHILE
	{
		posiciones.push(polaca.size());
	}	
;

print: PRINT '(' CADENACARACTERES ')' ';'   
	{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(35));
		polaca.add(((Token) $3.obj).getLexema());
		polaca.add(((Token) $1.obj).getLexema());
	}     
	| PRINT '(' CADENACARACTERES ')'
	{
		mensajes.error(analizador.getNroLinea() - 1, analizador.getMensaje(10));
	}		
	| PRINT '(' error
	{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(19));
	}							
	| error '(' CADENACARACTERES ')' ';'
	{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(20));
	}		
;

expresion: expresion '+' termino
	{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) $1.obj);
		lista.addAll((ArrayList<String>) $3.obj);
		lista.add(((Token) $2.obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		$$.obj = lista;
	}
	| expresion '-' termino
	{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) $1.obj);
		lista.addAll((ArrayList<String>) $3.obj);
		lista.add(((Token) $2.obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		$$.obj = lista;
	}
	| termino
	{
		$$.obj = $1.obj;
	}
	
;

termino: termino '*' tipo_factor
	{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) $1.obj);
		lista.addAll((ArrayList<String>) $3.obj);
		lista.add(((Token) $2.obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		$$.obj = lista;
	}
	| termino '/' tipo_factor
	{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) $1.obj);
		lista.addAll((ArrayList<String>) $3.obj);
		lista.add(((Token) $2.obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		$$.obj = lista;
	}
	| tipo_factor
	{
		$$.obj = $1.obj;
	}
;

tipo_factor: factor
	{
		$$.obj = $1.obj;
	}
	| factor_negativo
	{
		$$.obj = $1.obj;
	}
	| error
	{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(29));
		List<String> lista = new ArrayList<String>();
		$$.obj = lista;
	}
;

factor: CTEENTERO 
	{
		String lexema = ((Token) $1.obj).getLexema();
		Long e = Long.valueOf(lexema);
		if (e == Short.MAX_VALUE + 1){
			tabla.removeETS(lexema);
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(4));

		} else {
			List<String> lista = new ArrayList<String>();
			lista.add(lexema);
			$$.obj = lista;
		}
		
	}
	| CTEDOUBLE
	{
		Token token = (Token) $1.obj;
		List<String> lista = new ArrayList<String>();
		lista.add(token.getLexema());
		$$.obj = lista;
	}
	| id 
	{
		String id = (String) $1.obj;
		List<String> lista = new ArrayList<String>();
		lista.add(id);
		$$.obj = lista;
	}
	| objeto_var
	{
		String lexema = (String) $1.obj;
		List<String> lista = new ArrayList<String>();
		lista.add(lexema);
		$$.obj = lista;
	}      		
;

factor_negativo: '-' constante 
	{
		String lexema = ((Token) $2.obj).getLexema();
		String newLexema = '-' + lexema;
		List<String> lista = new ArrayList<String>();

		if (tabla.contieneLexema(lexema)){

			if (tabla.contieneLexema(newLexema)) 
				tabla.aumentarReferencia(newLexema);
			else {
				EntradaTS ets = new EntradaTS(tabla.getEntradaTS(lexema).getId(), newLexema);
				ets.setUso(tabla.getEntradaTS(lexema).getUso());
				ets.setTipo(tabla.getEntradaTS(lexema).getTipo());
				tabla.addETS(newLexema, ets);
			}
			if (tabla.ultimaReferencia(lexema))
				tabla.removeETS(lexema);
			else  
				tabla.disminuirReferencia(lexema);
		
		
		lista.add(newLexema);
		$$.obj = lista;

		} else {
			lista.add(null);
			$$.obj = lista;
		}	
		
	}
;

constante: CTEDOUBLE
	| CTEENTERO
;


%%

AnalizadorLexico analizador;
Mensajes mensajes;
TablaSimbolos tabla;


String ambito;
int cantOperaciones;
int cantMetodos;
private ArrayList<String> polaca;
private Stack<Integer> posiciones;   // Para el if y el while
private Stack<Integer> auxiliar;     // Para el if 


public Parser(){
	ambito = "Main";
	polaca = new ArrayList<String>();
	posiciones = new Stack<Integer>();
	auxiliar = new Stack<Integer>();
	cantOperaciones = 1;
	cantMetodos = 0;
}

void yyerror(String s)
{
	if (s.contains("under"))
 		System.out.println("par:"+s);
}

public void setLexico(AnalizadorLexico al) {
	analizador = al;
	tabla = al.getTablaDeSimbolos();
}

public void setMensajes(Mensajes ms) {
	mensajes = ms;
}

int yylex()
{
	int val = analizador.yylex(); 
    yylval = new ParserVal (analizador.getToken());
	yylval.ival = analizador.getNroLinea();

	return val;
}

public List<String> getPolaca(){
	return polaca;
}

public String obtenerAuxiliar(String tipo){
	String salida = "@aux" + cantOperaciones;  
	cantOperaciones ++;
	EntradaTS ets = new EntradaTS(salida, "Variable Auxiliar");
	ets.setTipo(tipo);
	tabla.addETS(salida, ets);
	return salida;
}

public String getTipoExpresion(List<String> l){
	for (String var: l)
		if (var != null && tabla.contieneLexema(var))
			if (tabla.getEntradaTS(var).getTipo() != null)
				if (tabla.getEntradaTS(var).getTipo().equals("double"))
					return "double";
	return "int";
}

public TablaSimbolos getTablaSimbolos(){
	return tabla;
}

public int getcantMetodos(){
	return cantMetodos;
}

public EntradaTS newEntradaTS(String lexema){
	EntradaTS aux = tabla.getEntradaTS(lexema);
	EntradaTS nueva = new EntradaTS(aux.getId(), aux.getLexema());
	nueva.setContRef(aux.getContRef());
	nueva.setTipo(aux.getTipo());
	nueva.setUso(aux.getUso());
	nueva.setVisibilidad(aux.getVisibilidad());
	nueva.setAmbito(aux.getAmbito());
	nueva.setPadre(aux.getPadre());
	return nueva;
}