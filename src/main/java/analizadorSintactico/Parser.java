//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 2 "gramatica.y"
package analizadorSintactico;
import analizadorLexico.EntradaTS;
import analizadorLexico.Token;
import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import compilador.Mensajes;
import java.util.ArrayList;
import java.util.Stack;
import java.util.List;
import java.util.HashMap;


//#line 30 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class ParserVal is defined in ParserVal.java


String   yytext;//user variable to return contextual strings
ParserVal yyval; //used to return semantic vals from action routines
ParserVal yylval;//the 'lval' (result) I got from yylex()
ParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new ParserVal[YYSTACKSIZE];
  yyval=new ParserVal();
  yylval=new ParserVal();
  valptr=-1;
}
void val_push(ParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
ParserVal val_pop()
{
  if (valptr<0)
    return new ParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
ParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new ParserVal();
  return valstk[ptr];
}
final ParserVal dup_yyval(ParserVal val)
{
  ParserVal dup = new ParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short IF=257;
public final static short ELSE=258;
public final static short END_IF=259;
public final static short PRINT=260;
public final static short INT=261;
public final static short BEGIN=262;
public final static short END=263;
public final static short DOUBLE=264;
public final static short FIN=265;
public final static short WHILE=266;
public final static short DO=267;
public final static short CLASS=268;
public final static short PUBLIC=269;
public final static short PRIVATE=270;
public final static short ID=271;
public final static short CTEENTERO=272;
public final static short CTEDOUBLE=273;
public final static short COMODIN=274;
public final static short COMPARADOR=275;
public final static short ASIGNACION=276;
public final static short CADENACARACTERES=277;
public final static short VOID=278;
public final static short EXTENDS=279;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    0,    0,    1,    1,    4,    4,    5,    5,
    5,    6,    9,    9,   10,   10,   11,   11,   12,   12,
   13,   13,   13,   15,   14,   14,    8,    8,    7,    7,
    7,    3,    3,   16,   16,   16,   16,   16,   16,   22,
   22,   22,   23,   18,   18,   18,   18,   18,   24,   17,
   17,   17,   17,   17,   17,   19,   19,   19,   19,   26,
   28,   29,   30,    2,    2,    2,   27,   27,   27,   27,
   20,   31,   21,   21,   21,   21,   25,   25,   25,   32,
   32,   32,   33,   33,   33,   34,   34,   34,   34,   35,
   36,   36,
};
final static short yylen[] = {                            2,
    3,    5,    3,    4,    2,    1,    1,    1,    3,    3,
    2,    5,    1,    3,    2,    1,    1,    1,    2,    1,
    4,    3,    3,    3,    1,    1,    3,    1,    1,    1,
    1,    2,    1,    1,    1,    1,    1,    1,    2,    3,
    3,    3,    1,    4,    4,    4,    3,    3,    1,    4,
    4,    3,    3,    3,    2,    4,    2,    4,    2,    3,
    1,    5,    1,    3,    3,    3,    1,    3,    3,    1,
    4,    1,    5,    4,    3,    5,    3,    3,    1,    3,
    3,    1,    1,    1,    1,    1,    1,    1,    1,    2,
    1,    1,
};
final static short yydefred[] = {                         0,
    0,   29,   30,    0,   31,    0,    0,    6,    7,    8,
    0,    0,    0,    0,    0,    0,    0,    5,    0,   28,
    0,    0,    0,    0,   72,    0,    0,    0,   33,   34,
   35,   36,   37,   38,    0,    0,    0,    0,    0,    0,
    0,    3,    0,    0,    1,   10,    9,    0,   39,    0,
    0,    0,    0,   61,    0,    0,    0,    0,    0,   86,
   87,    0,   43,   89,   88,    0,    0,   82,   83,   84,
    0,   32,    0,    0,    0,    0,   59,    0,   57,   63,
    0,   14,   25,   26,   20,    0,   16,   17,   18,    0,
    0,   65,    0,    0,   27,    0,   41,    0,    0,   70,
   67,   60,   75,    0,    0,   42,   40,   92,   91,   90,
   54,    0,    0,    0,    0,    2,   47,    0,   48,    0,
    0,    0,    0,   12,   15,    0,   19,    0,    0,    0,
    0,    0,    0,   45,    0,    0,   80,   81,   46,   44,
   51,   50,   58,   56,   71,   24,    0,    0,    0,   22,
   23,   76,    0,    0,   68,   73,    0,   21,   62,   64,
};
final static short yydgoto[] = {                          6,
    7,   17,   43,    8,    9,   10,   11,   21,   14,   86,
   87,   88,   89,   90,   91,   29,   30,   31,   32,   33,
   34,   35,   36,   37,   66,   38,  102,   53,   54,   81,
   39,   67,   68,   69,   70,  110,
};
final static short yysindex[] = {                       -12,
 -240,    0,    0, -227,    0,    0,  -58,    0,    0,    0,
 -228,  163, -243, -207,   91,  163, -183,    0,   14,    0,
  -11,   10,   50,   72,    0,  -14,   22,  103,    0,    0,
    0,    0,    0,    0,   13, -201, -160, -130,   50, -146,
   28,    0,  115,  127,    0,    0,    0, -140,    0, -143,
 -129,   22,   79,    0, -235,  102, -192,  110,  112,    0,
    0, -166,    0,    0,    0,   35,   30,    0,    0,    0,
 -116,    0,  -25,  107,   22,   22,    0,   79,    0,    0,
  -88,    0,    0,    0,    0,  -81,    0,    0,    0, -190,
   92,    0,   10,    0,    0,  143,    0,  -32,  163,    0,
    0,    0,    0,  144,  128,    0,    0,    0,    0,    0,
    0,   22,   22,   22,   22,    0,    0,  -59,    0,   58,
   59, -173,   79,    0,    0,  -66,    0,  -34, -210,  149,
   22,  139,  150,    0,   30,   30,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  163,  163, -210,    0,
    0,    0,   70,   10,    0,    0,  151,    0,    0,    0,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,  -51,    0,    0,    0,    0,    0,    0,    0,
  174,    0,    0,    0,    0,  -62,    0,    0,    0,    0,
    0,    0,    0,    0,  -60,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,  -39,  -18,    0,
    0,    0,    0,    0,    0, -136,    4,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0, -102,    1,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0, -106,
  -85,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,  -64,    0,   25,   46,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,   67,    0,    0,    0,    0,    0,    0,
};
final static short yygindex[] = {                         0,
    0, -110,   -7,  208,  -29,    0,    0,    0,    0,    0,
  137,    0,    0,    0,    0,  -13,    0,    0,    0,    0,
    0,  -17,   24,   33,  -38,    0,  -61,    0,  186,    0,
    0,   64,   86,    0,    0,    0,
};
final static int YYTABLESIZE=445;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                        140,
    4,   85,   85,   85,   28,   85,  149,   85,   44,   63,
  112,   85,  113,   98,   72,  118,  122,  150,  151,   85,
  103,   12,   49,   49,   49,   56,   49,   19,   49,   72,
   72,   57,   48,  117,   63,   40,  120,  121,  158,  101,
   49,  104,   20,   13,   79,  147,   79,   47,   79,   50,
   64,  148,   73,   74,   41,   51,   85,   63,   63,   65,
  127,  145,   79,  106,  101,   77,   62,   77,   49,   77,
    2,  114,   46,    3,   75,   64,  115,  112,  107,  113,
    5,   45,  143,   77,   65,  144,   78,  126,   78,   52,
   78,  132,  153,  111,   63,   63,   63,   63,   64,   64,
  112,  112,  113,  113,   78,  108,  109,   65,   65,  101,
  159,   55,  112,   63,  113,   76,  141,  142,   72,   55,
   55,   55,   55,   55,   82,   77,   55,   78,   79,   55,
   95,  128,  129,   96,   55,   64,   64,   64,   64,   55,
  157,   97,  105,   72,   65,   65,   65,   65,  116,   53,
   53,   53,   53,   53,   64,   51,   53,   57,   66,   53,
   66,   66,   66,   65,   53,  119,   66,   66,   66,   53,
   52,   52,   52,   52,   52,  135,  136,   52,  123,    2,
   52,  124,    3,  130,  133,   52,  134,   83,   84,    5,
   52,   74,   74,   74,   74,   74,  139,   15,   74,  137,
  138,   74,    2,   16,  146,    3,   74,  152,  156,    4,
   13,   74,    5,   49,   18,   43,   85,   85,   85,   85,
   85,  147,  125,   85,   80,    0,   85,  148,    0,    0,
    0,   85,    0,    0,    0,   85,   85,   49,   49,   49,
   49,   49,  131,    1,   49,    0,    0,   49,    2,    0,
    0,    3,   49,    0,    0,    4,   49,   49,    5,   79,
   79,   79,   79,   79,    0,   64,   79,    0,    0,   79,
    0,    0,    0,    0,   79,    0,    0,   58,   79,   79,
   77,   77,   77,   77,   77,    0,    0,   77,    2,    0,
   77,    3,   59,   60,   61,   77,   83,   84,    5,   77,
   77,   78,   78,   78,   78,   78,    0,    0,   78,    0,
    0,   78,    0,    0,    0,    0,   78,    0,    0,    0,
   78,   78,   69,   69,   69,   69,   69,    0,    0,   69,
    0,    0,   69,    0,   22,   23,    0,   69,   24,    0,
   99,  100,   69,    0,   25,    0,   22,   23,    0,   26,
   24,    0,    0,    0,   27,   42,   25,    0,   22,   23,
    0,   26,   24,    0,    0,   71,   27,    0,   25,    0,
   22,   23,    0,   26,   24,    0,    0,   92,   27,    0,
   25,    0,   93,   23,    0,   26,   24,    0,    0,   94,
   27,    0,   25,    0,  154,   23,    0,   26,   24,    0,
    0,  155,   27,    0,   25,    0,   93,   23,    0,   26,
   24,    0,    0,  160,   27,    0,   25,    0,   22,   23,
    0,   26,   24,    0,    0,    0,   27,    0,   25,   11,
    0,    0,    0,   26,   11,   11,   11,   11,   27,    0,
    0,   11,   11,   11,   11,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         59,
    0,   41,   42,   43,   12,   45,   41,   47,   16,   27,
   43,   41,   45,   52,   28,   41,   78,  128,  129,   59,
  256,  262,   41,   42,   43,   40,   45,  256,   47,   43,
   44,   46,   44,   59,   52,  279,   75,   76,  149,   53,
   59,  277,  271,  271,   41,  256,   43,   59,   45,   40,
   27,  262,   40,   41,  262,   46,   86,   75,   76,   27,
   90,  123,   59,  256,   78,   41,   45,   43,   59,   45,
  261,   42,   59,  264,  276,   52,   47,   43,  271,   45,
  271,  265,  256,   59,   52,  259,   41,  278,   43,   40,
   45,   99,  131,   59,  112,  113,  114,  115,   75,   76,
   43,   43,   45,   45,   59,  272,  273,   75,   76,  123,
   41,   40,   43,  131,   45,  276,   59,   59,  132,  256,
  257,  258,  259,  260,  271,  256,  263,  258,  259,  266,
  271,   40,   41,  277,  271,  112,  113,  114,  115,  276,
  148,  271,   41,  157,  112,  113,  114,  115,  265,  256,
  257,  258,  259,  260,  131,   46,  263,   46,  261,  266,
  263,  264,  265,  131,  271,   59,  269,  270,  271,  276,
  256,  257,  258,  259,  260,  112,  113,  263,  267,  261,
  266,  263,  264,   41,   41,  271,   59,  269,  270,  271,
  276,  256,  257,  258,  259,  260,  256,  256,  263,  114,
  115,  266,  261,  262,  271,  264,  271,   59,   59,  268,
  262,  276,  271,  276,    7,  276,  256,  257,  258,  259,
  260,  256,   86,  263,   39,   -1,  266,  262,   -1,   -1,
   -1,  271,   -1,   -1,   -1,  275,  276,  256,  257,  258,
  259,  260,  275,  256,  263,   -1,   -1,  266,  261,   -1,
   -1,  264,  271,   -1,   -1,  268,  275,  276,  271,  256,
  257,  258,  259,  260,   -1,  265,  263,   -1,   -1,  266,
   -1,   -1,   -1,   -1,  271,   -1,   -1,  256,  275,  276,
  256,  257,  258,  259,  260,   -1,   -1,  263,  261,   -1,
  266,  264,  271,  272,  273,  271,  269,  270,  271,  275,
  276,  256,  257,  258,  259,  260,   -1,   -1,  263,   -1,
   -1,  266,   -1,   -1,   -1,   -1,  271,   -1,   -1,   -1,
  275,  276,  256,  257,  258,  259,  260,   -1,   -1,  263,
   -1,   -1,  266,   -1,  256,  257,   -1,  271,  260,   -1,
  262,  263,  276,   -1,  266,   -1,  256,  257,   -1,  271,
  260,   -1,   -1,   -1,  276,  265,  266,   -1,  256,  257,
   -1,  271,  260,   -1,   -1,  263,  276,   -1,  266,   -1,
  256,  257,   -1,  271,  260,   -1,   -1,  263,  276,   -1,
  266,   -1,  256,  257,   -1,  271,  260,   -1,   -1,  263,
  276,   -1,  266,   -1,  256,  257,   -1,  271,  260,   -1,
   -1,  263,  276,   -1,  266,   -1,  256,  257,   -1,  271,
  260,   -1,   -1,  263,  276,   -1,  266,   -1,  256,  257,
   -1,  271,  260,   -1,   -1,   -1,  276,   -1,  266,  256,
   -1,   -1,   -1,  271,  261,  262,  263,  264,  276,   -1,
   -1,  268,  269,  270,  271,
};
}
final static short YYFINAL=6;
final static short YYMAXTOKEN=279;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,"'('","')'","'*'","'+'","','",
"'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,null,"';'",
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,"IF","ELSE","END_IF","PRINT","INT","BEGIN","END",
"DOUBLE","FIN","WHILE","DO","CLASS","PUBLIC","PRIVATE","ID","CTEENTERO",
"CTEDOUBLE","COMODIN","COMPARADOR","ASIGNACION","CADENACARACTERES","VOID",
"EXTENDS",
};
final static String yyrule[] = {
"$accept : program",
"program : declaraciones bloque_sentencias FIN",
"program : error BEGIN sentencias END FIN",
"program : declaraciones error FIN",
"program : declaraciones BEGIN sentencias END",
"declaraciones : declaraciones declaracion",
"declaraciones : declaracion",
"declaracion : variable",
"declaracion : clase",
"variable : tipo list_var ';'",
"variable : tipo error ';'",
"variable : tipo list_var",
"clase : CLASS encabezado_clase BEGIN sentencias_class END",
"encabezado_clase : ID",
"encabezado_clase : ID EXTENDS ID",
"sentencias_class : sentencias_class class_body",
"sentencias_class : class_body",
"class_body : clase_atributos",
"class_body : clase_metodos",
"clase_atributos : visibilidad variable",
"clase_atributos : variable",
"clase_metodos : encabezado_metodo '(' ')' bloque_sentencias",
"clase_metodos : encabezado_metodo '(' bloque_sentencias",
"clase_metodos : encabezado_metodo ')' bloque_sentencias",
"encabezado_metodo : visibilidad VOID ID",
"visibilidad : PUBLIC",
"visibilidad : PRIVATE",
"list_var : list_var ',' ID",
"list_var : ID",
"tipo : INT",
"tipo : DOUBLE",
"tipo : ID",
"sentencias : sentencias sentencia",
"sentencias : sentencia",
"sentencia : asignacion",
"sentencia : metodo",
"sentencia : seleccion",
"sentencia : iteracion",
"sentencia : print",
"sentencia : error ';'",
"objeto : ID '.' ID",
"objeto : error '.' ID",
"objeto : ID '.' error",
"objeto_var : objeto",
"metodo : objeto '(' ')' ';'",
"metodo : ID '(' ')' ';'",
"metodo : objeto '(' ')' error",
"metodo : objeto '(' ';'",
"metodo : objeto ')' ';'",
"id : ID",
"asignacion : id ASIGNACION expresion ';'",
"asignacion : objeto_var ASIGNACION expresion ';'",
"asignacion : id ASIGNACION expresion",
"asignacion : objeto_var ASIGNACION expresion",
"asignacion : ASIGNACION expresion ';'",
"asignacion : ASIGNACION expresion",
"seleccion : seleccion_simple ELSE bloque END_IF",
"seleccion : seleccion_simple END_IF",
"seleccion : seleccion_simple ELSE bloque error",
"seleccion : seleccion_simple error",
"seleccion_simple : IF condicion_if bloque",
"condicion_if : condicion",
"condicion : '(' expresion COMPARADOR expresion ')'",
"condicion_while : condicion",
"bloque_sentencias : BEGIN sentencias END",
"bloque_sentencias : error sentencias END",
"bloque_sentencias : BEGIN sentencias error",
"bloque : sentencia",
"bloque : BEGIN sentencias END",
"bloque : BEGIN sentencias error",
"bloque : END",
"iteracion : while condicion_while DO bloque",
"while : WHILE",
"print : PRINT '(' CADENACARACTERES ')' ';'",
"print : PRINT '(' CADENACARACTERES ')'",
"print : PRINT '(' error",
"print : error '(' CADENACARACTERES ')' ';'",
"expresion : expresion '+' termino",
"expresion : expresion '-' termino",
"expresion : termino",
"termino : termino '*' tipo_factor",
"termino : termino '/' tipo_factor",
"termino : tipo_factor",
"tipo_factor : factor",
"tipo_factor : factor_negativo",
"tipo_factor : error",
"factor : CTEENTERO",
"factor : CTEDOUBLE",
"factor : id",
"factor : objeto_var",
"factor_negativo : '-' constante",
"constante : CTEDOUBLE",
"constante : CTEENTERO",
};

//#line 801 "gramatica.y"

AnalizadorLexico analizador;
Mensajes mensajes;
TablaSimbolos tabla;


String ambito;
int cantOperaciones;
int cantMetodos;
private ArrayList<String> polaca;
private Stack<Integer> posiciones;   // Para el if y el while
private Stack<Integer> auxiliar;     // Para el if 


public Parser(){
	ambito = "Main";
	polaca = new ArrayList<String>();
	posiciones = new Stack<Integer>();
	auxiliar = new Stack<Integer>();
	cantOperaciones = 1;
	cantMetodos = 0;
}

void yyerror(String s)
{
	if (s.contains("under"))
 		System.out.println("par:"+s);
}

public void setLexico(AnalizadorLexico al) {
	analizador = al;
	tabla = al.getTablaDeSimbolos();
}

public void setMensajes(Mensajes ms) {
	mensajes = ms;
}

int yylex()
{
	int val = analizador.yylex(); 
    yylval = new ParserVal (analizador.getToken());
	yylval.ival = analizador.getNroLinea();

	return val;
}

public List<String> getPolaca(){
	return polaca;
}

public String obtenerAuxiliar(String tipo){
	String salida = "@aux" + cantOperaciones;  
	cantOperaciones ++;
	EntradaTS ets = new EntradaTS(salida, "Variable Auxiliar");
	ets.setTipo(tipo);
	tabla.addETS(salida, ets);
	return salida;
}

public String getTipoExpresion(List<String> l){
	for (String var: l)
		if (var != null && tabla.contieneLexema(var))
			if (tabla.getEntradaTS(var).getTipo() != null)
				if (tabla.getEntradaTS(var).getTipo().equals("double"))
					return "double";
	return "int";
}

public TablaSimbolos getTablaSimbolos(){
	return tabla;
}

public int getcantMetodos(){
	return cantMetodos;
}

public EntradaTS newEntradaTS(String lexema){
	EntradaTS aux = tabla.getEntradaTS(lexema);
	EntradaTS nueva = new EntradaTS(aux.getId(), aux.getLexema());
	nueva.setContRef(aux.getContRef());
	nueva.setTipo(aux.getTipo());
	nueva.setUso(aux.getUso());
	nueva.setVisibilidad(aux.getVisibilidad());
	nueva.setAmbito(aux.getAmbito());
	nueva.setPadre(aux.getPadre());
	return nueva;
}
//#line 525 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 22 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(41));		
	}
break;
case 2:
//#line 26 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(9));
	}
break;
case 3:
//#line 30 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(8));
	}
break;
case 4:
//#line 34 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(7));
	}
break;
case 9:
//#line 48 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(31));
		List<Token> aux = new ArrayList<>();

		for (Token e: ((List<Token>) val_peek(1).obj)){
			if (tabla.declarada(e.getLexema())){
				e.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(50)+": " + e.getLexema());
				/*variable redeclarada*/
			}
			else {	
				String tipo = ((String) val_peek(2).obj);
				aux.add(e);																			
				if (tipo.equals("int") || tipo.equals("double")){
					e.getETS().setTipo(tipo);
					e.getETS().setAmbito(ambito);
					e.getETS().setUso("Nombre de Variable");
				} else {
					if (tabla.declarada(tipo)){
						e.getETS().setTipo(tipo);
						e.getETS().setAmbito(ambito);
						if (ambito.equals("Main"))
							e.getETS().setUso("Nombre de Objeto");							
						while (tipo != null){
							for (String l: tabla.getEntradaTS(tipo).getListaAtributos()){
								tabla.addETS(e.getLexema() + "@" + l, newEntradaTS(l));
							}	
							tipo = tabla.getEntradaTS(tipo).getPadre(); 
						}
					} else 
						tabla.removeETS(e.getLexema());
				}
			}	
		}
	
		yyval.obj = ((List<Token>) aux);
	}
break;
case 10:
//#line 86 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(12));
		yyval.obj = new ArrayList<Token>();
	}
break;
case 11:
//#line 91 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
		yyval.obj = new ArrayList<Token>();
	}
break;
case 12:
//#line 98 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(37));
		String lexema = (String) val_peek(3).obj;
		tabla.getEntradaTS(lexema).setListaAtributos((List<String>) val_peek(1).obj);
		ambito = "Main";
	}
break;
case 13:
//#line 107 "gramatica.y"
{
		Token token = (Token) val_peek(0).obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema()))
			{
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(51)+": " + token.getLexema());
				/*variable redeclarada*/
			}
			else {																																																																
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
			}
		yyval.obj = token.getLexema();
	}
break;
case 14:
//#line 123 "gramatica.y"
{
		Token token= (Token) val_peek(2).obj;
		Token token2 = (Token) val_peek(0).obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema())){
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(51)+": " + token.getLexema());
				/*variable redeclarada*/
				if (token.getLexema().equals(token2.getLexema()))
					token2.getETS().decContRef();
				else
					if (tabla.ultimaReferencia(token2.getLexema()))
						tabla.removeETS(token2.getLexema());
					else
						token2.getETS().decContRef();
			}
			else {		
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
				if (tabla.declarada(token2.getLexema())){
					token2.getETS().decContRef();
					token.getETS().setPadre(token2.getLexema());
				} else{
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token2.getLexema());
					tabla.removeETS(token2.getLexema());
				}
			}
		yyval.obj = token.getLexema();
	}
break;
case 15:
//#line 155 "gramatica.y"
{
		List<String> aux = (List<String>) val_peek(1).obj;
		aux.addAll((List<String>) val_peek(0).obj);
		yyval.obj = aux;
	}
break;
case 16:
//#line 161 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 17:
//#line 167 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 18:
//#line 171 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 19:
//#line 177 "gramatica.y"
{
		List<String> lexemas = new ArrayList<>();
		for (Token e: ((List<Token>) val_peek(0).obj)){
			e.getETS().setUso("Nombre de Atributo");
			e.getETS().setVisibilidad((((Token) val_peek(1).obj).getLexema()).toLowerCase());
			lexemas.add(e.getLexema());
		}
		yyval.obj = lexemas;
	}
break;
case 20:
//#line 187 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(26));
		List<String> lexemas = new ArrayList<>();
		yyval.obj = lexemas;
	}
break;
case 21:
//#line 195 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(38));
		List<String> lexemas = new ArrayList<>();
		if (val_peek(3).obj != null){
			lexemas.add((String) val_peek(3).obj);
			polaca.add("FM");
			cantMetodos++;
		}
		yyval.obj = lexemas;
	}
break;
case 22:
//#line 206 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23));
	}
break;
case 23:
//#line 210 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22));
	}
break;
case 24:
//#line 216 "gramatica.y"
{
	Token token = (Token) val_peek(0).obj;
	yyval.obj = null;
	if (tabla.declarada(token.getLexema())){
		token.getETS().decContRef();
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(53)+": " + token.getLexema());
	} else {
		token.getETS().setTipo((((Token) val_peek(1).obj).getLexema()).toLowerCase());																								
		token.getETS().setUso("Nombre de Metodo");	
		token.getETS().setVisibilidad((((Token) val_peek(2).obj).getLexema()).toLowerCase());
		token.getETS().setAmbito(ambito);

		polaca.add(token.getLexema());
		polaca.add("IM");

		yyval.obj = token.getLexema();
	}
}
break;
case 27:
//#line 240 "gramatica.y"
{
		List<Token> tokens = (List<Token>) val_peek(2).obj;
		Token token = (Token) val_peek(0).obj;
		tokens.add(token);
		yyval.obj = tokens;
	}
break;
case 28:
//#line 247 "gramatica.y"
{
		List<Token> tokens = new ArrayList<Token>();
		Token token = (Token)val_peek(0).obj;
		tokens.add(token);
		yyval.obj = tokens;
	}
break;
case 29:
//#line 256 "gramatica.y"
{
		yyval.obj = ((Token) val_peek(0).obj).getLexema().toLowerCase();
	}
break;
case 30:
//#line 260 "gramatica.y"
{
		yyval.obj = ((Token) val_peek(0).obj).getLexema().toLowerCase();
	}
break;
case 31:
//#line 264 "gramatica.y"
{
		Token token = (Token) val_peek(0).obj;
		if (!tabla.declarada(token.getLexema())){
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
		yyval.obj = token.getLexema();
	}
break;
case 35:
//#line 280 "gramatica.y"
{
		String objeto = (String) val_peek(0).obj;
		if (objeto != null){
			polaca.add(objeto);
			polaca.add("LM");
		}
	}
break;
case 39:
//#line 291 "gramatica.y"
{
	  mensajes.error(analizador.getNroLinea(),analizador.getMensaje(21));
	}
break;
case 40:
//#line 297 "gramatica.y"
{
		Token tokenObj = (Token) val_peek(2).obj;
		Token tokenAtr = (Token) val_peek(0).obj;
		yyval.obj = null;
		if (ambito.equals("Main")){
			if (!tabla.declarada(tokenObj.getLexema())){
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + tokenObj.getLexema());
				tabla.removeETS(tokenObj.getLexema());
				if (tabla.ultimaReferencia(tokenAtr.getLexema()))
					tabla.removeETS(tokenAtr.getLexema());
				else
					tabla.disminuirReferencia(tokenAtr.getLexema());
			} else {
				if (!tabla.declarada(tokenAtr.getLexema())){
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(55)+": " + tokenAtr.getLexema());
					tabla.removeETS(tokenAtr.getLexema());
				} else {
					String lexema = tokenObj.getLexema() + "@" + tokenAtr.getLexema();
					if (!tabla.contieneLexema(lexema))
						mensajes.error(analizador.getNroLinea(), analizador.getMensaje(56)+": " + tokenObj.getLexema());
					else
						yyval.obj = (String) lexema;
				}
			}
		}
		else {
			mensajes.error(analizador.getNroLinea(), analizador.getMensaje(59));
			if (tabla.declarada(tokenObj.getLexema()))
				tabla.disminuirReferencia(tokenObj.getLexema());
			else 
				tabla.removeETS(tokenObj.getLexema());
			if (tabla.declarada(tokenAtr.getLexema()))
				tabla.disminuirReferencia(tokenAtr.getLexema());
			else 
				tabla.removeETS(tokenAtr.getLexema());
		}	
	}
break;
case 41:
//#line 335 "gramatica.y"
{
		String atr = ((Token) val_peek(0).obj).getLexema();
		yyval.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(24));
		if (tabla.declarada(atr))
			tabla.disminuirReferencia(atr);
		else 
			tabla.removeETS(atr);
	}
break;
case 42:
//#line 345 "gramatica.y"
{
		String obj = ((Token) val_peek(2).obj).getLexema();
		yyval.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(25));
		if (tabla.declarada(obj))
			tabla.disminuirReferencia(obj);
		else 
			tabla.removeETS(obj);
	}
break;
case 43:
//#line 357 "gramatica.y"
{
		String objeto = (String) val_peek(0).obj;
		yyval.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Atributo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(61)+": " + aux[1]);
				yyval.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(57)+": " + aux[1]);
					yyval.obj = null;
				}
			}
		}
	}
break;
case 44:
//#line 376 "gramatica.y"
{
		String objeto = (String) val_peek(3).obj;
		yyval.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Metodo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(60)+": " + aux[1]);
				yyval.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(58)+": " + aux[1]);
					yyval.obj = null;
				}
			}
		}
		
	}
break;
case 45:
//#line 394 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(40));
		Token token = (Token) val_peek(3).obj;
		yyval.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso();
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Metodo")){
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							yyval.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(58)+": " + token.getLexema());
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema());	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema());
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(60)+": " + token.getLexema());
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema());
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
	}
break;
case 46:
//#line 437 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(10));
	}
break;
case 47:
//#line 441 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23));
	}
break;
case 48:
//#line 445 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22));
	}
break;
case 49:
//#line 451 "gramatica.y"
{
		Token token = (Token) val_peek(0).obj;
		yyval.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso(); 
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Variable")) {
					if (ambitoLocal.equals(ambito))
						yyval.obj = (String) token.getLexema();
				} else if (uso.equals("Nombre de Atributo")) {
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							yyval.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(57)+": " + token.getLexema());
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema());	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema());
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(61)+": " + token.getLexema());
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema());
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema());
			tabla.removeETS(token.getLexema());
		}
	}
break;
case 50:
//#line 498 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(32));
		polaca.addAll((ArrayList<String>) val_peek(1).obj);
		polaca.add((String) val_peek(3).obj);
		polaca.add(":=");
	}
break;
case 51:
//#line 505 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(39));
		polaca.addAll((ArrayList<String>) val_peek(1).obj);
		polaca.add((String) val_peek(3).obj);
		polaca.add(":=");
	}
break;
case 52:
//#line 512 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
	}
break;
case 53:
//#line 516 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10));
	}
break;
case 54:
//#line 520 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(13));
	}
break;
case 55:
//#line 524 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(14));
	}
break;
case 56:
//#line 530 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		if(!auxiliar.isEmpty())
			auxiliar.pop();
		int nroPilaInicial = posiciones.pop();
		polaca.remove(nroPilaInicial);
		polaca.add(nroPilaInicial, String.valueOf(polaca.size()+1));
	}
break;
case 57:
//#line 539 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		polaca.remove(polaca.size()-1);
		posiciones.pop();
		polaca.remove(polaca.size()-1);
		int pos = auxiliar.pop();
		int val = Integer.valueOf(polaca.get(pos));
		polaca.remove(pos);
		polaca.add(pos, String.valueOf(val-2));
	}
break;
case 58:
//#line 550 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27));
	}
break;
case 59:
//#line 554 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27));
	}
break;
case 60:
//#line 560 "gramatica.y"
{
		int nroPilaInicial = posiciones.pop();
		auxiliar.push(nroPilaInicial);
		polaca.remove(nroPilaInicial);
		polaca.add(nroPilaInicial, String.valueOf(polaca.size()+3));
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BI");
	}
break;
case 61:
//#line 572 "gramatica.y"
{
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BF");
	}
break;
case 62:
//#line 580 "gramatica.y"
{
		polaca.addAll((ArrayList<String>) val_peek(3).obj);
		polaca.addAll((ArrayList<String>) val_peek(1).obj);
		polaca.add(((Token) val_peek(2).obj).getLexema());
	}
break;
case 63:
//#line 588 "gramatica.y"
{
		polaca.add("");
		posiciones.push(polaca.size()-1);
		polaca.add("BF");
	}
break;
case 64:
//#line 596 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(36));
	}
break;
case 65:
//#line 600 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15));
	}
break;
case 66:
//#line 604 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16));
	}
break;
case 69:
//#line 612 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16));
	}
break;
case 70:
//#line 616 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15));
	}
break;
case 71:
//#line 622 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(34));
		int nrobifurcacion = posiciones.pop();
		polaca.remove(nrobifurcacion);
		polaca.add(nrobifurcacion, String.valueOf(polaca.size()+3));
		int nroPilaInicial = posiciones.pop();
		polaca.add(String.valueOf(nroPilaInicial));
		polaca.add("BI");
	}
break;
case 72:
//#line 634 "gramatica.y"
{
		posiciones.push(polaca.size());
	}
break;
case 73:
//#line 640 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(35));
		polaca.add(((Token) val_peek(2).obj).getLexema());
		polaca.add(((Token) val_peek(4).obj).getLexema());
	}
break;
case 74:
//#line 646 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1, analizador.getMensaje(10));
	}
break;
case 75:
//#line 650 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(19));
	}
break;
case 76:
//#line 654 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(20));
	}
break;
case 77:
//#line 660 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) val_peek(2).obj);
		lista.addAll((ArrayList<String>) val_peek(0).obj);
		lista.add(((Token) val_peek(1).obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 78:
//#line 669 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) val_peek(2).obj);
		lista.addAll((ArrayList<String>) val_peek(0).obj);
		lista.add(((Token) val_peek(1).obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 79:
//#line 678 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 80:
//#line 685 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) val_peek(2).obj);
		lista.addAll((ArrayList<String>) val_peek(0).obj);
		lista.add(((Token) val_peek(1).obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 81:
//#line 694 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) val_peek(2).obj);
		lista.addAll((ArrayList<String>) val_peek(0).obj);
		lista.add(((Token) val_peek(1).obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 82:
//#line 703 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 83:
//#line 709 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 84:
//#line 713 "gramatica.y"
{
		yyval.obj = val_peek(0).obj;
	}
break;
case 85:
//#line 717 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(29));
		List<String> lista = new ArrayList<String>();
		yyval.obj = lista;
	}
break;
case 86:
//#line 725 "gramatica.y"
{
		String lexema = ((Token) val_peek(0).obj).getLexema();
		Long e = Long.valueOf(lexema);
		if (e == Short.MAX_VALUE + 1){
			tabla.removeETS(lexema);
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(4));

		} else {
			List<String> lista = new ArrayList<String>();
			lista.add(lexema);
			yyval.obj = lista;
		}
		
	}
break;
case 87:
//#line 740 "gramatica.y"
{
		Token token = (Token) val_peek(0).obj;
		List<String> lista = new ArrayList<String>();
		lista.add(token.getLexema());
		yyval.obj = lista;
	}
break;
case 88:
//#line 747 "gramatica.y"
{
		String id = (String) val_peek(0).obj;
		List<String> lista = new ArrayList<String>();
		lista.add(id);
		yyval.obj = lista;
	}
break;
case 89:
//#line 754 "gramatica.y"
{
		String lexema = (String) val_peek(0).obj;
		List<String> lista = new ArrayList<String>();
		lista.add(lexema);
		yyval.obj = lista;
	}
break;
case 90:
//#line 763 "gramatica.y"
{
		String lexema = ((Token) val_peek(0).obj).getLexema();
		String newLexema = '-' + lexema;
		List<String> lista = new ArrayList<String>();

		if (tabla.contieneLexema(lexema)){

			if (tabla.contieneLexema(newLexema)) 
				tabla.aumentarReferencia(newLexema);
			else {
				EntradaTS ets = new EntradaTS(tabla.getEntradaTS(lexema).getId(), newLexema);
				ets.setUso(tabla.getEntradaTS(lexema).getUso());
				ets.setTipo(tabla.getEntradaTS(lexema).getTipo());
				tabla.addETS(newLexema, ets);
			}
			if (tabla.ultimaReferencia(lexema))
				tabla.removeETS(lexema);
			else  
				tabla.disminuirReferencia(lexema);
		
		
		lista.add(newLexema);
		yyval.obj = lista;

		} else {
			lista.add(null);
			yyval.obj = lista;
		}	
		
	}
break;
//#line 1514 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */



/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
