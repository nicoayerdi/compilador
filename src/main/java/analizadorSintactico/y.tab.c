#ifndef lint
static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";
#endif
#define YYBYACC 1
#line 2 "gramatica.y"
package analizadorSintactico;
import analizadorLexico.EntradaTS;
import analizadorLexico.Token;
import analizadorLexico.AnalizadorLexico;
import analizadorLexico.TablaSimbolos;
import compilador.Mensajes;
import java.util.ArrayList;
import java.util.Stack;
import java.util.List;
import java.util.HashMap;


#line 19 "y.tab.c"
#define IF 257
#define ELSE 258
#define END_IF 259
#define PRINT 260
#define INT 261
#define BEGIN 262
#define END 263
#define DOUBLE 264
#define FIN 265
#define WHILE 266
#define DO 267
#define CLASS 268
#define PUBLIC 269
#define PRIVATE 270
#define ID 271
#define CTEENTERO 272
#define CTEDOUBLE 273
#define COMODIN 274
#define COMPARADOR 275
#define ASIGNACION 276
#define CADENACARACTERES 277
#define VOID 278
#define EXTENDS 279
#define YYERRCODE 256
short yylhs[] = {                                        -1,
    0,    0,    0,    0,    1,    1,    4,    4,    5,    5,
    5,    6,    9,    9,   10,   10,   11,   11,   12,   12,
   13,   13,   13,   14,   14,    8,    8,    7,    7,    7,
    3,    3,   15,   15,   15,   15,   15,   15,   21,   21,
   21,   22,   17,   17,   17,   17,   17,   23,   16,   16,
   16,   16,   16,   16,   18,   18,   18,   18,   25,   27,
   28,   29,    2,    2,    2,   26,   26,   26,   26,   19,
   30,   20,   20,   20,   20,   24,   24,   24,   31,   31,
   31,   32,   32,   33,   33,   33,   33,   34,   35,   35,
};
short yylen[] = {                                         2,
    3,    5,    3,    4,    2,    1,    1,    1,    3,    3,
    2,    5,    1,    3,    2,    1,    1,    1,    2,    1,
    6,    5,    5,    1,    1,    3,    1,    1,    1,    1,
    2,    1,    1,    1,    1,    1,    1,    2,    3,    3,
    3,    1,    4,    4,    4,    3,    3,    1,    4,    4,
    3,    3,    3,    2,    4,    2,    4,    2,    3,    1,
    5,    1,    3,    3,    3,    1,    3,    3,    1,    4,
    1,    5,    4,    3,    5,    3,    3,    1,    3,    3,
    1,    1,    1,    1,    1,    1,    1,    2,    1,    1,
};
short yydefred[] = {                                      0,
    0,   28,   29,    0,   30,    0,    0,    6,    7,    8,
    0,    0,    0,    0,    0,    0,    0,    5,    0,   27,
    0,    0,    0,    0,   71,    0,    0,    0,   32,   33,
   34,   35,   36,   37,    0,    0,    0,    0,    0,    0,
    0,    3,    0,    0,    1,   10,    9,    0,   38,    0,
    0,    0,    0,   60,    0,    0,    0,    0,    0,   84,
   85,    0,   42,   87,   86,    0,    0,   81,   82,   83,
    0,   31,    0,    0,    0,    0,   58,    0,   56,   62,
    0,   14,   24,   25,   20,    0,   16,   17,   18,    0,
   64,    0,    0,   26,    0,   40,    0,    0,   69,   66,
   59,   74,    0,    0,   41,   39,   90,   89,   88,   53,
    0,    0,    0,    0,    2,   46,    0,   47,    0,    0,
    0,    0,   12,   15,    0,   19,    0,    0,    0,    0,
   44,    0,    0,   79,   80,   45,   43,   50,   49,   57,
   55,   70,    0,   75,    0,    0,   67,   72,    0,    0,
   61,    0,    0,    0,   22,   23,    0,   21,   63,
};
short yydgoto[] = {                                       6,
    7,   17,   43,    8,    9,   10,   11,   21,   14,   86,
   87,   88,   89,   90,   29,   30,   31,   32,   33,   34,
   35,   36,   37,   66,   38,  101,   53,   54,   81,   39,
   67,   68,   69,   70,  109,
};
short yysindex[] = {                                   -158,
 -240,    0,    0, -221,    0,    0,  133,    0,    0,    0,
 -225,  127, -224, -191,   50,  127, -156,    0,   53,    0,
   55,   -6,   96,  104,    0,    2,  -21,   67,    0,    0,
    0,    0,    0,    0,   37, -124, -121,  -12,   96, -137,
    7,    0,   79,   91,    0,    0,    0, -110,    0, -114,
  -98,  -21,   42,    0, -241,  135, -151,  128,  136,    0,
    0, -142,    0,    0,    0,  -26,   -4,    0,    0,    0,
  -81,    0,  -15,  122,  -21,  -21,    0,   42,    0,    0,
  -73,    0,    0,    0,    0,  -84,    0,    0,    0, -171,
    0,   -6,    0,    0,  154,    0,  -32,  127,    0,    0,
    0,    0,  157,  142,    0,    0,    0,    0,    0,    0,
  -21,  -21,  -21,  -21,    0,    0,  -59,    0,   24,   94,
 -238,   42,    0,    0,  -69,    0,  144,  -21,  103,  151,
    0,   -4,   -4,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   48,    0,  188,   -6,    0,    0,  -34, -202,
    0,  127,  127, -202,    0,    0,  115,    0,    0,
};
short yyrindex[] = {                                      0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,  -51,    0,    0,    0,    0,    0,    0,    0,
  -56,    0,    0,    0,    0,  -60,    0,    0,    0,    0,
    0,    0,    0,    0,  -53,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  -39,    0,
    0,    0,    0,    0,    0, -184,  -18,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0, -105,    1,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0, -131, -109,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  -88,
    0,    4,   25,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  -67,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,
};
short yygindex[] = {                                      0,
    0,   76,   -7,  227,  -25,    0,    0,    0,    0,    0,
  163,    0,    0,    0,  -14,    0,    0,    0,    0,    0,
  -17,    5,   10,  -24,    0,  -66,    0,  215,    0,    0,
  -10,   28,    0,    0,    0,
};
#define YYTABLESIZE 404
short yytable[] = {                                     137,
    4,   48,   48,   48,   28,   48,  154,   48,   44,   63,
  111,  121,  112,   72,  102,   85,  111,  140,  112,   48,
  141,   12,   78,   62,   78,  117,   78,   97,   72,   72,
   19,   64,  110,   50,   63,  103,   65,  113,  100,   51,
   78,   56,  114,  116,   76,   20,   76,   57,   76,   13,
  119,  120,   49,  152,   40,  142,   64,   63,   63,  153,
   85,   65,   76,  100,  126,   77,  111,   77,  112,   77,
   41,   54,   54,   54,   54,   54,   73,   74,   54,   64,
   64,   54,  138,   77,   65,   65,   54,  149,  150,    2,
  129,   54,    3,   63,   63,   63,   63,    1,   48,    5,
  132,  133,    2,  145,  105,    3,  125,  100,   45,    4,
   63,   46,    5,   47,   72,   64,   64,   64,   64,  106,
   65,   65,   65,   65,   52,   52,   52,   52,   52,  107,
  108,   52,   64,   82,   52,   52,  111,   65,  112,   52,
  134,  135,   72,   55,   52,  157,   51,   51,   51,   51,
   51,   75,  139,   51,   76,   65,   51,   65,   65,   65,
   94,   51,   95,   65,   65,   65,   51,   73,   73,   73,
   73,   73,   96,   51,   73,  104,    2,   73,  123,    3,
  118,   57,   73,  115,   83,   84,    5,   73,   68,   68,
   68,   68,   68,  122,  127,   68,  136,  130,   68,   11,
  131,  143,  144,   68,   11,   11,   11,   11,   68,  148,
   13,   11,   11,   11,   11,   48,   48,   48,   48,   48,
   48,  152,   42,   48,  155,  156,   48,  153,  151,  158,
  111,   48,  112,   18,   58,   48,   48,   78,   78,   78,
   78,   78,  128,   77,   78,   78,   79,   78,  124,   59,
   60,   61,   78,   80,    0,    0,   78,   78,    0,   76,
   76,   76,   76,   76,    0,   63,   76,    2,    0,   76,
    3,    0,    0,    0,   76,   83,   84,    5,   76,   76,
   77,   77,   77,   77,   77,    0,    0,   77,    0,    0,
   77,    0,    0,    0,    0,   77,    0,   22,   23,   77,
   77,   24,    0,   98,   99,   22,   23,   25,    0,   24,
    0,    0,   26,    0,   42,   25,    0,   27,    0,    0,
   26,    0,   22,   23,    0,   27,   24,    0,    0,   71,
    0,    0,   25,    0,   22,   23,    0,   26,   24,    0,
    0,   91,   27,    0,   25,    0,   92,   23,    0,   26,
   24,    0,    0,   93,   27,    0,   25,    0,  146,   23,
    0,   26,   24,    0,    0,  147,   27,    0,   25,    0,
   92,   23,    0,   26,   24,    0,    0,  159,   27,    0,
   25,    0,   22,   23,    0,   26,   24,    0,   15,    0,
   27,    0,   25,    2,   16,    0,    3,   26,    0,    0,
    4,    0,   27,    5,
};
short yycheck[] = {                                      59,
    0,   41,   42,   43,   12,   45,   41,   47,   16,   27,
   43,   78,   45,   28,  256,   41,   43,  256,   45,   59,
  259,  262,   41,   45,   43,   41,   45,   52,   43,   44,
  256,   27,   59,   40,   52,  277,   27,   42,   53,   46,
   59,   40,   47,   59,   41,  271,   43,   46,   45,  271,
   75,   76,   59,  256,  279,  122,   52,   75,   76,  262,
   86,   52,   59,   78,   90,   41,   43,   43,   45,   45,
  262,  256,  257,  258,  259,  260,   40,   41,  263,   75,
   76,  266,   59,   59,   75,   76,  271,   40,   41,  261,
   98,  276,  264,  111,  112,  113,  114,  256,   44,  271,
  111,  112,  261,  128,  256,  264,  278,  122,  265,  268,
  128,   59,  271,   59,  129,  111,  112,  113,  114,  271,
  111,  112,  113,  114,  256,  257,  258,  259,  260,  272,
  273,  263,  128,  271,  266,   40,   43,  128,   45,  271,
  113,  114,  157,   40,  276,  153,  256,  257,  258,  259,
  260,  276,   59,  263,  276,  261,  266,  263,  264,  265,
  271,  271,  277,  269,  270,  271,  276,  256,  257,  258,
  259,  260,  271,   46,  263,   41,  261,  266,  263,  264,
   59,   46,  271,  265,  269,  270,  271,  276,  256,  257,
  258,  259,  260,  267,   41,  263,  256,   41,  266,  256,
   59,  271,   59,  271,  261,  262,  263,  264,  276,   59,
  262,  268,  269,  270,  271,  276,  256,  257,  258,  259,
  260,  256,  276,  263,  149,  150,  266,  262,   41,  154,
   43,  271,   45,    7,  256,  275,  276,  256,  257,  258,
  259,  260,  275,  256,  263,  258,  259,  266,   86,  271,
  272,  273,  271,   39,   -1,   -1,  275,  276,   -1,  256,
  257,  258,  259,  260,   -1,  265,  263,  261,   -1,  266,
  264,   -1,   -1,   -1,  271,  269,  270,  271,  275,  276,
  256,  257,  258,  259,  260,   -1,   -1,  263,   -1,   -1,
  266,   -1,   -1,   -1,   -1,  271,   -1,  256,  257,  275,
  276,  260,   -1,  262,  263,  256,  257,  266,   -1,  260,
   -1,   -1,  271,   -1,  265,  266,   -1,  276,   -1,   -1,
  271,   -1,  256,  257,   -1,  276,  260,   -1,   -1,  263,
   -1,   -1,  266,   -1,  256,  257,   -1,  271,  260,   -1,
   -1,  263,  276,   -1,  266,   -1,  256,  257,   -1,  271,
  260,   -1,   -1,  263,  276,   -1,  266,   -1,  256,  257,
   -1,  271,  260,   -1,   -1,  263,  276,   -1,  266,   -1,
  256,  257,   -1,  271,  260,   -1,   -1,  263,  276,   -1,
  266,   -1,  256,  257,   -1,  271,  260,   -1,  256,   -1,
  276,   -1,  266,  261,  262,   -1,  264,  271,   -1,   -1,
  268,   -1,  276,  271,
};
#define YYFINAL 6
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 279
#if YYDEBUG
char *yyname[] = {
"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,"'('","')'","'*'","'+'","','","'-'","'.'","'/'",0,0,0,0,0,0,0,0,0,0,
0,"';'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,"IF","ELSE","END_IF","PRINT","INT","BEGIN","END","DOUBLE","FIN","WHILE","DO",
"CLASS","PUBLIC","PRIVATE","ID","CTEENTERO","CTEDOUBLE","COMODIN","COMPARADOR",
"ASIGNACION","CADENACARACTERES","VOID","EXTENDS",
};
char *yyrule[] = {
"$accept : program",
"program : declaraciones bloque_sentencias FIN",
"program : error BEGIN sentencias END FIN",
"program : declaraciones error FIN",
"program : declaraciones BEGIN sentencias END",
"declaraciones : declaraciones declaracion",
"declaraciones : declaracion",
"declaracion : variable",
"declaracion : clase",
"variable : tipo list_var ';'",
"variable : tipo error ';'",
"variable : tipo list_var",
"clase : CLASS encabezado_clase BEGIN sentencias_class END",
"encabezado_clase : ID",
"encabezado_clase : ID EXTENDS ID",
"sentencias_class : sentencias_class class_body",
"sentencias_class : class_body",
"class_body : clase_atributos",
"class_body : clase_metodos",
"clase_atributos : visibilidad variable",
"clase_atributos : variable",
"clase_metodos : visibilidad VOID ID '(' ')' bloque_sentencias",
"clase_metodos : visibilidad VOID ID '(' bloque_sentencias",
"clase_metodos : visibilidad VOID ID ')' bloque_sentencias",
"visibilidad : PUBLIC",
"visibilidad : PRIVATE",
"list_var : list_var ',' ID",
"list_var : ID",
"tipo : INT",
"tipo : DOUBLE",
"tipo : ID",
"sentencias : sentencias sentencia",
"sentencias : sentencia",
"sentencia : asignacion",
"sentencia : metodo",
"sentencia : seleccion",
"sentencia : iteracion",
"sentencia : print",
"sentencia : error ';'",
"objeto : ID '.' ID",
"objeto : error '.' ID",
"objeto : ID '.' error",
"objeto_var : objeto",
"metodo : objeto '(' ')' ';'",
"metodo : ID '(' ')' ';'",
"metodo : objeto '(' ')' error",
"metodo : objeto '(' ';'",
"metodo : objeto ')' ';'",
"id : ID",
"asignacion : id ASIGNACION expresion ';'",
"asignacion : objeto_var ASIGNACION expresion ';'",
"asignacion : id ASIGNACION expresion",
"asignacion : objeto_var ASIGNACION expresion",
"asignacion : ASIGNACION expresion ';'",
"asignacion : ASIGNACION expresion",
"seleccion : seleccion_simple ELSE bloque END_IF",
"seleccion : seleccion_simple END_IF",
"seleccion : seleccion_simple ELSE bloque error",
"seleccion : seleccion_simple error",
"seleccion_simple : IF condicion_if bloque",
"condicion_if : condicion",
"condicion : '(' expresion COMPARADOR expresion ')'",
"condicion_while : condicion",
"bloque_sentencias : BEGIN sentencias END",
"bloque_sentencias : error sentencias END",
"bloque_sentencias : BEGIN sentencias error",
"bloque : sentencia",
"bloque : BEGIN sentencias END",
"bloque : BEGIN sentencias error",
"bloque : END",
"iteracion : while condicion_while DO bloque",
"while : WHILE",
"print : PRINT '(' CADENACARACTERES ')' ';'",
"print : PRINT '(' CADENACARACTERES ')'",
"print : PRINT '(' error",
"print : error '(' CADENACARACTERES ')' ';'",
"expresion : expresion '+' termino",
"expresion : expresion '-' termino",
"expresion : termino",
"termino : termino '*' tipo_factor",
"termino : termino '/' tipo_factor",
"termino : tipo_factor",
"tipo_factor : factor",
"tipo_factor : factor_negativo",
"factor : CTEENTERO",
"factor : CTEDOUBLE",
"factor : id",
"factor : objeto_var",
"factor_negativo : '-' constante",
"constante : CTEDOUBLE",
"constante : CTEENTERO",
};
#endif
#ifndef YYSTYPE
typedef int YYSTYPE;
#endif
#define yyclearin (yychar=(-1))
#define yyerrok (yyerrflag=0)
#ifdef YYSTACKSIZE
#ifndef YYMAXDEPTH
#define YYMAXDEPTH YYSTACKSIZE
#endif
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 500
#define YYMAXDEPTH 500
#endif
#endif
int yydebug;
int yynerrs;
int yyerrflag;
int yychar;
short *yyssp;
YYSTYPE *yyvsp;
YYSTYPE yyval;
YYSTYPE yylval;
short yyss[YYSTACKSIZE];
YYSTYPE yyvs[YYSTACKSIZE];
#define yystacksize YYSTACKSIZE
#line 800 "gramatica.y"

AnalizadorLexico analizador;
Mensajes mensajes;
TablaSimbolos tabla;


String ambito;
int cantOperaciones;
private ArrayList<String> pila;
private ArrayList<String> pilaAuxiliar;
private Stack<Integer> posiciones;   // Para el if y el while
private Stack<Integer> auxiliar;     // Para el if 

private HashMap<String,List<Token>> objetos;

public Parser(){
	ambito = "Main";
	pila = new ArrayList<String>();
	pilaAuxiliar = new ArrayList<String>();
	posiciones = new Stack<Integer>();
	auxiliar = new Stack<Integer>();
	objetos = new HashMap<>();
	cantOperaciones = 1;
}

void yyerror(String s)
{
	if (s.contains("under"))
 		System.out.println("par:"+s);
}

public void setLexico(AnalizadorLexico al) {
	analizador = al;
	tabla = al.getTablaDeSimbolos();
}

public void setMensajes(Mensajes ms) {
	mensajes = ms;
}

int yylex()
{
	int val = analizador.yylex(); 
    yylval = new ParserVal (analizador.getToken());
	yylval.ival = analizador.getNroLinea();

	return val;
}

public List<String> getPila(){
	return pila;
}

public String obtenerAuxiliar(String tipo){
	String salida = "@aux" + cantOperaciones;  
	cantOperaciones ++;
	EntradaTS ets = new EntradaTS(salida, "Variable Auxiliar");
	ets.setTipo(tipo);
	tabla.addETS(salida, ets);
	return salida;
}

public String getTipoExpresion(List<String> l){
	for (String var: l)
		if (var != null && tabla.contieneLexema(var))
			if (tabla.getEntradaTS(var).getTipo() != null)
				if (tabla.getEntradaTS(var).getTipo().equals("double"))
					return "double";
	return "int";
}

public TablaSimbolos getTablaSimbolos(){
	return tabla;
}
#line 434 "y.tab.c"
#define YYABORT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR goto yyerrlab
int
yyparse()
{
    register int yym, yyn, yystate;
#if YYDEBUG
    register char *yys;
    extern char *getenv();

    if (yys = getenv("YYDEBUG"))
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = (-1);

    yyssp = yyss;
    yyvsp = yyvs;
    *yyssp = yystate = 0;

yyloop:
    if (yyn = yydefred[yystate]) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("yydebug: state %d, reading %d (%s)\n", yystate,
                    yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("yydebug: state %d, shifting to state %d (%s)\n",
                    yystate, yytable[yyn],yyrule[yyn]);
#endif
        if (yyssp >= yyss + yystacksize - 1)
        {
            goto yyoverflow;
        }
        *++yyssp = yystate = yytable[yyn];
        *++yyvsp = yylval;
        yychar = (-1);
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;
#ifdef lint
    goto yynewerror;
#endif
yynewerror:
    yyerror("syntax error");
#ifdef lint
    goto yyerrlab;
#endif
yyerrlab:
    ++yynerrs;
yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yyssp]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("yydebug: state %d, error recovery shifting\
 to state %d\n", *yyssp, yytable[yyn]);
#endif
                if (yyssp >= yyss + yystacksize - 1)
                {
                    goto yyoverflow;
                }
                *++yyssp = yystate = yytable[yyn];
                *++yyvsp = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("yydebug: error recovery discarding state %d\n",
                            *yyssp);
#endif
                if (yyssp <= yyss) goto yyabort;
                --yyssp;
                --yyvsp;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("yydebug: state %d, error recovery discards token %d (%s)\n",
                    yystate, yychar, yys);
        }
#endif
        yychar = (-1);
        goto yyloop;
    }
yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("yydebug: state %d, reducing by rule %d (%s)\n",
                yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    yyval = yyvsp[1-yym];
    switch (yyn)
    {
case 1:
#line 22 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(41));
		pila.addAll(pilaAuxiliar);
	}
break;
case 2:
#line 27 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(9),"SINTACTICO");
	}
break;
case 3:
#line 31 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(8),"SINTACTICO");
	}
break;
case 4:
#line 35 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(7),"SINTACTICO");
	}
break;
case 9:
#line 49 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(31));
		List<Token> aux = new ArrayList<>();

		for (Token e: ((List<Token>) yyvsp[-1].obj)){
			if (tabla.declarada(e.getLexema())){
				e.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(50)+": " + e.getLexema(),"SEMANTICO");
				/*variable redeclarada*/
			}
			else {	
				String tipo = ((String) yyvsp[-2].obj);
				aux.add(e);																			
				if (tipo.equals("int") || tipo.equals("double")){
					e.getETS().setTipo(tipo);
					e.getETS().setAmbito(ambito);
					e.getETS().setUso("Nombre de Variable");
				} else {
					if (tabla.declarada(tipo)){
						e.getETS().setTipo(tipo);
						e.getETS().setAmbito(ambito);
						if (ambito.equals("Main"))
							e.getETS().setUso("Nombre de Objeto");							
						while (tipo != null){
							for (Token l: objetos.get(tipo)){
								if (l.getETS().getUso().equals("Nombre de Atributo")){
									EntradaTS ets = new EntradaTS(l.getETS().getId(), l.getETS().getLexema());
									ets.setTipo(l.getETS().getTipo());
									ets.setUso("Atributo de Objeto");
									ets.setVisibilidad(l.getETS().getVisibilidad());
									ets.setAmbito(l.getETS().getAmbito());
									ets.setPadre(l.getETS().getPadre());
									ets.setPilaMetodo(l.getETS().getPilaMetodo());
									tabla.addETS(e.getLexema() + "@" + l.getLexema(), ets);
								}
								else
									tabla.addETS(e.getLexema() + "@" + l.getLexema(), l.getETS());
							}	
							tipo = tabla.getEntradaTS(tipo).getPadre(); 
						}
					} else 
						tabla.removeETS(e.getLexema());
				}
			}	
		}
	
		yyval.obj = ((List<Token>) aux);
	}
break;
case 10:
#line 98 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(12),"SINTACTICO");
	}
break;
case 11:
#line 102 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10),"SINTACTICO");
	}
break;
case 12:
#line 108 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(37));
		/* System.out.println((List<Token>) $4.obj);*/
		Token token = (Token) yyvsp[-3].obj;
		if (!objetos.containsKey(token.getLexema()))
			objetos.put(token.getLexema(), (List<Token>) yyvsp[-1].obj);
		ambito = "Main";
	}
break;
case 13:
#line 119 "gramatica.y"
{
		Token token = (Token) yyvsp[0].obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema()))
			{
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(51)+": " + token.getLexema(),"SEMANTICO");
				/*variable redeclarada*/
			}
			else {																																																																
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
			}
		yyval.obj = token;
	}
break;
case 14:
#line 135 "gramatica.y"
{
		Token token= (Token) yyvsp[-2].obj;
		Token token2 = (Token) yyvsp[0].obj;
		ambito = token.getLexema();
			if (tabla.declarada(token.getLexema())){
				token.getETS().decContRef();
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(51)+": " + token.getLexema(),"SEMANTICO");
				/*variable redeclarada*/
				if (token.getLexema().equals(token2.getLexema()))
					token2.getETS().decContRef();
				else
					if (tabla.ultimaReferencia(token2.getLexema()))
						tabla.removeETS(token2.getLexema());
					else
						token2.getETS().decContRef();
			}
			else {		
				token.getETS().setTipo(token.getLexema());																								
				token.getETS().setUso("Nombre de Clase");		
				if (tabla.declarada(token2.getLexema())){
					token2.getETS().decContRef();
					token.getETS().setPadre(token2.getLexema());
				} else{
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token2.getLexema(),"SEMANTICO");
					tabla.removeETS(token2.getLexema());
				}
			}
		yyval.obj = token;
	}
break;
case 15:
#line 167 "gramatica.y"
{
		List<Token> aux = (List<Token>) yyvsp[-1].obj;
		aux.addAll((List<Token>) yyvsp[0].obj);
		yyval.obj = (List<Token>) aux;
	}
break;
case 16:
#line 173 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 17:
#line 179 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 18:
#line 183 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 19:
#line 189 "gramatica.y"
{
		for (Token e: ((List<Token>) yyvsp[0].obj)){
			e.getETS().setUso("Nombre de Atributo");
			e.getETS().setVisibilidad((((Token) yyvsp[-1].obj).getLexema()).toLowerCase());
		}
		yyval.obj = (List<Token>) yyvsp[0].obj;
	}
break;
case 20:
#line 197 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(26),"SINTACTICO");
	}
break;
case 21:
#line 203 "gramatica.y"
{
		List<Token> aux = new ArrayList<>();
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(38));
		Token token = (Token) yyvsp[-3].obj;
		if (tabla.declarada(token.getLexema())){
			token.getETS().decContRef();
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(53)+": " + token.getLexema(),"SEMANTICO");
		} else {
			token.getETS().setTipo((((Token) yyvsp[-4].obj).getLexema()).toLowerCase());																								
			token.getETS().setUso("Nombre de Metodo");	
			token.getETS().setVisibilidad((((Token) yyvsp[-5].obj).getLexema()).toLowerCase());
			token.getETS().setAmbito(ambito);
			aux.add(token);
			token.getETS().setPilaMetodo(pilaAuxiliar);
			pilaAuxiliar.clear();
		}
		yyval.obj = (List<Token>) aux;
	}
break;
case 22:
#line 222 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23),"SINTACTICO");
	}
break;
case 23:
#line 226 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22),"SINTACTICO");
	}
break;
case 26:
#line 236 "gramatica.y"
{
		List<Token> tokens = (List<Token>) yyvsp[-2].obj;
		Token token = (Token) yyvsp[0].obj;
		tokens.add(token);
		yyval.obj = tokens;
	}
break;
case 27:
#line 243 "gramatica.y"
{
		List<Token> tokens = new ArrayList<Token>();
		Token token = (Token)yyvsp[0].obj;
		tokens.add(token);
		yyval.obj = tokens;
	}
break;
case 28:
#line 252 "gramatica.y"
{
		yyval.obj = ((Token) yyvsp[0].obj).getLexema().toLowerCase();
	}
break;
case 29:
#line 256 "gramatica.y"
{
		yyval.obj = ((Token) yyvsp[0].obj).getLexema().toLowerCase();
	}
break;
case 30:
#line 260 "gramatica.y"
{
		Token token = (Token) yyvsp[0].obj;
		if (!tabla.declarada(token.getLexema())){
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + token.getLexema(),"SEMANTICO");
			tabla.removeETS(token.getLexema());
		}
		yyval.obj = token.getLexema();
	}
break;
case 34:
#line 276 "gramatica.y"
{
		String objeto = (String) yyvsp[0].obj;
		if (objeto != null){
			List<String> list;
			if (objeto.contains("@")){
				String[] aux = objeto.split("@");
				list = tabla.getEntradaTS(aux[1]).getPilaMetodo();

				for (int y=0; y < list.size(); y++)
					if (list.get(y) != null && tabla.contieneLexema(list.get(y)) && tabla.getEntradaTS(list.get(y)).getUso().equals("Nombre de Atributo"))
						list.set(y, String.valueOf(aux[0] + "@" + list.get(y)));
				
			} else
				list = tabla.getEntradaTS(objeto).getPilaMetodo();
				
			for (int x=0; x < list.size(); x++){
				if (list.get(x) != null && (list.get(x).equals("BF") || list.get(x).equals("BI"))){
					String posicion = list.get(x-1);
					if (posicion != null && !posicion.equals(""))
						list.set(x-1, String.valueOf(Integer.valueOf(posicion) + pilaAuxiliar.size()));
				}	
			}	
			pilaAuxiliar.addAll(list);
		}
	}
break;
case 38:
#line 305 "gramatica.y"
{
	  mensajes.error(analizador.getNroLinea(),analizador.getMensaje(21),"SINTACTICO");
	}
break;
case 39:
#line 311 "gramatica.y"
{
		Token tokenObj = (Token) yyvsp[-2].obj;
		Token tokenAtr = (Token) yyvsp[0].obj;
		yyval.obj = null;
		if (ambito.equals("Main")){
			if (!tabla.declarada(tokenObj.getLexema())){
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(52)+": " + tokenObj.getLexema(),"SEMANTICO");
				tabla.removeETS(tokenObj.getLexema());
				if (tabla.ultimaReferencia(tokenAtr.getLexema()))
					tabla.removeETS(tokenAtr.getLexema());
				else
					tabla.disminuirReferencia(tokenAtr.getLexema());
			} else {
				if (!tabla.declarada(tokenAtr.getLexema())){
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(55)+": " + tokenAtr.getLexema(),"SEMANTICO");
					tabla.removeETS(tokenAtr.getLexema());
				} else {
					String lexema = tokenObj.getLexema() + "@" + tokenAtr.getLexema();
					if (!tabla.contieneLexema(lexema))
						mensajes.error(analizador.getNroLinea(), analizador.getMensaje(56)+": " + tokenObj.getLexema(),"SEMANTICO");
					else
						yyval.obj = (String) lexema;
				}
			}
		}
		else {
			mensajes.error(analizador.getNroLinea(), analizador.getMensaje(59),"SEMANTICO");
			if (tabla.declarada(tokenObj.getLexema()))
				tabla.disminuirReferencia(tokenObj.getLexema());
			else 
				tabla.removeETS(tokenObj.getLexema());
			if (tabla.declarada(tokenAtr.getLexema()))
				tabla.disminuirReferencia(tokenAtr.getLexema());
			else 
				tabla.removeETS(tokenAtr.getLexema());
		}	
	}
break;
case 40:
#line 349 "gramatica.y"
{
		String atr = ((Token) yyvsp[0].obj).getLexema();
		yyval.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(24),"SINTACTICO");
		if (tabla.declarada(atr))
			tabla.disminuirReferencia(atr);
		else 
			tabla.removeETS(atr);
	}
break;
case 41:
#line 359 "gramatica.y"
{
		String obj = ((Token) yyvsp[-2].obj).getLexema();
		yyval.obj = null;
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(25),"SINTACTICO");
		if (tabla.declarada(obj))
			tabla.disminuirReferencia(obj);
		else 
			tabla.removeETS(obj);
	}
break;
case 42:
#line 371 "gramatica.y"
{
		String objeto = (String) yyvsp[0].obj;
		yyval.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Atributo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(61)+": " + aux[1],"SEMANTICO");
				yyval.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(57)+": " + aux[1],"SEMANTICO");
					yyval.obj = null;
				}
			}
		}
	}
break;
case 43:
#line 390 "gramatica.y"
{
		String objeto = (String) yyvsp[-3].obj;
		yyval.obj = (String) objeto;
		if (objeto != null){
			String[] aux = objeto.split("@");
			if (!tabla.getEntradaTS(aux[1]).getUso().equals("Nombre de Metodo")){
				mensajes.error(analizador.getNroLinea(), analizador.getMensaje(60)+": " + aux[1],"SEMANTICO");
				yyval.obj = null;
			} else {
				if (tabla.getEntradaTS(aux[1]).getVisibilidad().equals("private")){
					mensajes.error(analizador.getNroLinea(), analizador.getMensaje(58)+": " + aux[1],"SEMANTICO");
					yyval.obj = null;
				}
			}
		}
		
	}
break;
case 44:
#line 408 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(40));
		Token token = (Token) yyvsp[-3].obj;
		yyval.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso();
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Metodo")){
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							yyval.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(58)+": " + token.getLexema(),"SEMANTICO");
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema(),"SEMANTICO");	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(64)+": " + token.getLexema(),"SEMANTICO");
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(60)+": " + token.getLexema(),"SEMANTICO");
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema(),"SEMANTICO");
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(65)+": " + token.getLexema(),"SEMANTICO");
			tabla.removeETS(token.getLexema());
		}
	}
break;
case 45:
#line 451 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(10),"SINTACTICO");
	}
break;
case 46:
#line 455 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(23),"SINTACTICO");
	}
break;
case 47:
#line 459 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(22),"SINTACTICO");
	}
break;
case 48:
#line 465 "gramatica.y"
{
		Token token = (Token) yyvsp[0].obj;
		yyval.obj = null;
		if (tabla.declarada(token.getLexema())){
			String uso = tabla.getEntradaTS(token.getLexema()).getUso(); 
			if (uso != null) {
				String ambitoLocal = tabla.getEntradaTS(token.getLexema()).getAmbito();
				if (uso.equals("Nombre de Variable")) {
					if (ambitoLocal.equals(ambito))
						yyval.obj = (String) token.getLexema();
				} else if (uso.equals("Nombre de Atributo")) {
					if (ambitoLocal.equals(ambito)){
						yyval.obj = (String) token.getLexema();
					} else if (!ambito.equals("Main")){
						String padre = tabla.getEntradaTS(ambito).getPadre();
						String visibilidad = tabla.getEntradaTS(token.getLexema()).getVisibilidad();
						while (padre != null && !padre.equals(ambitoLocal))
							padre = tabla.getEntradaTS(padre).getPadre();
						if (padre != null && !visibilidad.equals("private"))
							yyval.obj = (String) token.getLexema();
						else if (padre != null)
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(57)+": " + token.getLexema(),"SEMANTICO");
						else	
							mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema(),"SEMANTICO");	
					} else {
						mensajes.error(analizador.getNroLinea(),analizador.getMensaje(63)+": " + token.getLexema(),"SEMANTICO");
						tabla.disminuirReferencia(token.getLexema());
					}
				} else {
					mensajes.error(analizador.getNroLinea(),analizador.getMensaje(61)+": " + token.getLexema(),"SEMANTICO");
					tabla.disminuirReferencia(token.getLexema());
				}
			} else {
				mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema(),"SEMANTICO");
				if (tabla.ultimaReferencia(token.getLexema()))
					tabla.removeETS(token.getLexema());
				else
					tabla.disminuirReferencia(token.getLexema());
			}
		} else {
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(62)+": " + token.getLexema(),"SEMANTICO");
			tabla.removeETS(token.getLexema());
		}
	}
break;
case 49:
#line 512 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(32));
		pilaAuxiliar.addAll((ArrayList<String>) yyvsp[-1].obj);
		pilaAuxiliar.add((String) yyvsp[-3].obj);
		pilaAuxiliar.add(":=");
	}
break;
case 50:
#line 519 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(39));
		pilaAuxiliar.addAll((ArrayList<String>) yyvsp[-1].obj);
		pilaAuxiliar.add((String) yyvsp[-3].obj);
		pilaAuxiliar.add(":=");
	}
break;
case 51:
#line 526 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10),"SINTACTICO");
	}
break;
case 52:
#line 530 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1,analizador.getMensaje(10),"SINTACTICO");
	}
break;
case 53:
#line 534 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(13),"SINTACTICO");
	}
break;
case 54:
#line 538 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(14),"SINTACTICO");
	}
break;
case 55:
#line 544 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		if(!auxiliar.isEmpty())
			auxiliar.pop();
		int nroPilaInicial = posiciones.pop();
		pilaAuxiliar.remove(nroPilaInicial);
		pilaAuxiliar.add(nroPilaInicial, String.valueOf(pilaAuxiliar.size()+1));
	}
break;
case 56:
#line 553 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(33));
		pilaAuxiliar.remove(pilaAuxiliar.size()-1);
		posiciones.pop();
		pilaAuxiliar.remove(pilaAuxiliar.size()-1);
		int pos = auxiliar.pop();
		int val = Integer.valueOf(pilaAuxiliar.get(pos));
		pilaAuxiliar.remove(pos);
		pilaAuxiliar.add(pos, String.valueOf(val-2));
	}
break;
case 57:
#line 564 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27),"SINTACTICO");
	}
break;
case 58:
#line 568 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(27),"SINTACTICO");
	}
break;
case 59:
#line 574 "gramatica.y"
{
		int nroPilaInicial = posiciones.pop();
		auxiliar.push(nroPilaInicial);
		pilaAuxiliar.remove(nroPilaInicial);
		pilaAuxiliar.add(nroPilaInicial, String.valueOf(pilaAuxiliar.size()+3));
		pilaAuxiliar.add("");
		posiciones.push(pilaAuxiliar.size()-1);
		pilaAuxiliar.add("BI");
	}
break;
case 60:
#line 586 "gramatica.y"
{
		pilaAuxiliar.add("");
		posiciones.push(pilaAuxiliar.size()-1);
		pilaAuxiliar.add("BF");
	}
break;
case 61:
#line 594 "gramatica.y"
{
		pilaAuxiliar.addAll((ArrayList<String>) yyvsp[-3].obj);
		pilaAuxiliar.addAll((ArrayList<String>) yyvsp[-1].obj);
		pilaAuxiliar.add(((Token) yyvsp[-2].obj).getLexema());
	}
break;
case 62:
#line 602 "gramatica.y"
{
		pilaAuxiliar.add("");
		posiciones.push(pilaAuxiliar.size()-1);
		pilaAuxiliar.add("BF");
	}
break;
case 63:
#line 610 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(36));
	}
break;
case 64:
#line 614 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15),"SINTACTICO");
	}
break;
case 65:
#line 618 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16),"SINTACTICO");
	}
break;
case 68:
#line 626 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(16),"SINTACTICO");
	}
break;
case 69:
#line 630 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(),analizador.getMensaje(15),"SINTACTICO");
	}
break;
case 70:
#line 636 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(34));
		int nrobifurcacion = posiciones.pop();
		pilaAuxiliar.remove(nrobifurcacion);
		pilaAuxiliar.add(nrobifurcacion, String.valueOf(pilaAuxiliar.size()+3));
		int nroPilaInicial = posiciones.pop();
		pilaAuxiliar.add(String.valueOf(nroPilaInicial));
		pilaAuxiliar.add("BI");
	}
break;
case 71:
#line 648 "gramatica.y"
{
		posiciones.push(pilaAuxiliar.size());
	}
break;
case 72:
#line 654 "gramatica.y"
{
		mensajes.estructuraSintactica(analizador.getNroLinea(), analizador.getMensaje(35));
		pilaAuxiliar.add(((Token) yyvsp[-2].obj).getLexema());
		pilaAuxiliar.add(((Token) yyvsp[-4].obj).getLexema());
	}
break;
case 73:
#line 660 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea() - 1, analizador.getMensaje(10), "SINTACTICO");
	}
break;
case 74:
#line 664 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(19), "SINTACTICO");
	}
break;
case 75:
#line 668 "gramatica.y"
{
		mensajes.error(analizador.getNroLinea(), analizador.getMensaje(20), "SINTACTICO");
	}
break;
case 76:
#line 674 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) yyvsp[-2].obj);
		lista.addAll((ArrayList<String>) yyvsp[0].obj);
		lista.add(((Token) yyvsp[-1].obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 77:
#line 683 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) yyvsp[-2].obj);
		lista.addAll((ArrayList<String>) yyvsp[0].obj);
		lista.add(((Token) yyvsp[-1].obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 78:
#line 692 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 79:
#line 698 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) yyvsp[-2].obj);
		lista.addAll((ArrayList<String>) yyvsp[0].obj);
		lista.add(((Token) yyvsp[-1].obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 80:
#line 707 "gramatica.y"
{
		List<String> lista = new ArrayList<String>();
		lista.addAll((ArrayList<String>) yyvsp[-2].obj);
		lista.addAll((ArrayList<String>) yyvsp[0].obj);
		lista.add(((Token) yyvsp[-1].obj).getLexema());
		lista.add(obtenerAuxiliar(getTipoExpresion(lista)));
		yyval.obj = lista;
	}
break;
case 81:
#line 716 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 82:
#line 722 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 83:
#line 726 "gramatica.y"
{
		yyval.obj = yyvsp[0].obj;
	}
break;
case 84:
#line 732 "gramatica.y"
{
		String lexema = ((Token) yyvsp[0].obj).getLexema();
		Long e = Long.valueOf(lexema);
		if (e == Short.MAX_VALUE + 1){
			tabla.removeETS(lexema);
			mensajes.error(analizador.getNroLinea(),analizador.getMensaje(4),"SINTACTICO");

		} else {
			List<String> lista = new ArrayList<String>();
			lista.add(lexema);
			yyval.obj = lista;
		}
		
	}
break;
case 85:
#line 747 "gramatica.y"
{
		Token token = (Token) yyvsp[0].obj;
		List<String> lista = new ArrayList<String>();
		lista.add(token.getLexema());
		yyval.obj = lista;
	}
break;
case 86:
#line 754 "gramatica.y"
{
		String id = (String) yyvsp[0].obj;
		List<String> lista = new ArrayList<String>();
		lista.add(id);
		yyval.obj = lista;
	}
break;
case 87:
#line 761 "gramatica.y"
{
		String lexema = (String) yyvsp[0].obj;
		List<String> lista = new ArrayList<String>();
		lista.add(lexema);
		yyval.obj = lista;
	}
break;
case 88:
#line 770 "gramatica.y"
{
		String lexema = ((Token) yyvsp[0].obj).getLexema();
		String newLexema = '-' + lexema;

		if (tabla.contieneLexema(newLexema)) 
			tabla.aumentarReferencia(newLexema);
		else {
			EntradaTS ets = new EntradaTS(tabla.getEntradaTS(lexema).getId(), newLexema);
			ets.setUso(tabla.getEntradaTS(lexema).getUso());
			ets.setTipo(tabla.getEntradaTS(lexema).getTipo());
			tabla.addETS(newLexema, ets);
		}
		if (tabla.ultimaReferencia(lexema))
			tabla.removeETS(lexema);
		else  
			tabla.disminuirReferencia(lexema);
		
		List<String> lista = new ArrayList<String>();
		lista.add(newLexema);
		yyval.obj = lista;
		
	}
break;
#line 1411 "y.tab.c"
    }
    yyssp -= yym;
    yystate = *yyssp;
    yyvsp -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("yydebug: after reduction, shifting from state 0 to\
 state %d\n", YYFINAL);
#endif
        yystate = YYFINAL;
        *++yyssp = YYFINAL;
        *++yyvsp = yyval;
        if (yychar < 0)
        {
            if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("yydebug: state %d, reading %d (%s)\n",
                        YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("yydebug: after reduction, shifting from state %d \
to state %d\n", *yyssp, yystate);
#endif
    if (yyssp >= yyss + yystacksize - 1)
    {
        goto yyoverflow;
    }
    *++yyssp = yystate;
    *++yyvsp = yyval;
    goto yyloop;
yyoverflow:
    yyerror("yacc stack overflow");
yyabort:
    return (1);
yyaccept:
    return (0);
}
